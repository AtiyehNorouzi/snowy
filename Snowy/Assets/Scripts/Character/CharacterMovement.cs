﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
/*
 * Character movement
 * this script contains movement of the character (jump, running, doublejump)
 * written by Smokey Shadow
 */
public class CharacterMovement : MonoBehaviour
{

    #region CONST FIELDS
    const float rotateDegree = -360;
    const float MAX_VELOCITY = 3;
    const string HIT_MASK = "mountain";
    const float HIT_DIST = 0.7f;
    const float MIN_Y_ALIVE = -505f;

    #endregion

    #region FIELDS

    [SerializeField]
    private Rigidbody2D _character_rb;

    private bool _canDoubleJump;

    private RaycastHit2D _hit;

    private bool _firstJump = false;

    #endregion

    #region MONOBEHABIOUR
    void FixedUpdate()
    {

        if (transform.position.y <= MIN_Y_ALIVE)
            CharacterDeath();


        if (CharacterState.Instance.GetState() == CharacterState.State.Running)
            _character_rb.velocity = Vector3.ClampMagnitude(_character_rb.velocity, MAX_VELOCITY);
        else if (CharacterState.Instance.GetState() == CharacterState.State.Boost)
            _character_rb.velocity = Vector3.ClampMagnitude(_character_rb.velocity, MAX_VELOCITY + 2);


        _hit = Physics2D.Raycast(transform.position, -Vector2.up, HIT_DIST, LayerMask.GetMask(HIT_MASK));
        if (_hit.collider != null)
        {

            if (Input.GetKeyDown(KeyCode.Space) || Input.GetMouseButtonDown(0))
            {
                Jump();

                if (!_firstJump)
                {
                    _firstJump = true;
                    _character_rb.constraints = RigidbodyConstraints2D.None;
                }
            }
            else
            {
                if ((CharacterState.Instance.GetState() != CharacterState.State.Running && CharacterState.Instance.GetState() != CharacterState.State.Boost)
                    && _firstJump && _character_rb.velocity.y <= 0)
                {
                    Land();
                }
            }

        }
        else
        {
            if (Input.GetKeyDown(KeyCode.Space) || Input.GetMouseButtonDown(0))
            {
                if (_canDoubleJump)
                {
                    AddForce(Vector3.up, 17f);
                    _canDoubleJump = false;
                }
            }
        }
    }

    #endregion

    #region PUBLIC METHODS
    public void AddForce(Vector3 direction, float amount)
    {
        _character_rb.AddForce(direction * amount, ForceMode2D.Impulse);
    }

    public float GetVelocity()
    {
        return _character_rb.velocity.magnitude;
    }

    public void FreezeCharacter()
    {
        if (_firstJump)
        {
            CharacterState.Instance.ChangeState(CharacterState.State.NoMove);
            _character_rb.constraints = RigidbodyConstraints2D.FreezeAll;
        }
    }
    public void DeFreezeCharacter()
    {
        CharacterState.Instance.ChangeState(CharacterState.State.Running);
        _character_rb.constraints = RigidbodyConstraints2D.None;
    }

    public void CharacterDeath()
    {
        StartCoroutine(DeadRoutine());
    }
    #endregion

    #region PRIVATE METHODS
    private void Jump()
    {
        _character_rb.velocity = new Vector2(_character_rb.velocity.x, 0);
        AddForce(Vector3.up, 25f);
        _canDoubleJump = true;
        CharacterState.Instance.ChangeState(CharacterState.State.Jump);
        SoundPlayer.Instance.PlaySound(SoundPlayer.SoundClip.Jump);
        CharacterParticleControl.Instance.PlayParticle(CharacterParticleControl.ParticleType.JumpUp, transform);
    }

    private void Land()
    {
        CharacterState.Instance.ChangeState(CharacterState.State.Running);
        CharacterParticleControl.Instance.PlayParticle(CharacterParticleControl.ParticleType.JumpDown, transform);
        SoundPlayer.Instance.PlaySound(SoundPlayer.SoundClip.Land);
    }

    IEnumerator DeadRoutine()
    {
        CharacterState.Instance.ChangeState(CharacterState.State.Dead);
        SoundPlayer.Instance.PlaySound(SoundPlayer.SoundClip.PlayerDead);
        yield return new WaitForSeconds(3f);
        SceneManager.LoadScene(2);
    }

    #endregion
}