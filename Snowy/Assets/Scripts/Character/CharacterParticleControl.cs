﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterParticleControl : MonoBehaviour
{
    #region FIELDS
    private static CharacterParticleControl _instance;

    [SerializeField]
    private ParticleSystem[] particles;

    private ParticleType _particleType;

    private float _playTimer = 0;
    #endregion

    #region ENUM
    public enum ParticleType { JumpUp = 0, JumpDown, OnCollideSnow, OnCollideSheild, Die, Boost }
    #endregion

    #region PROPERTIES
    public static CharacterParticleControl Instance
    {
        get
        {
            if (_instance == null)
                _instance = GameObject.FindObjectOfType<CharacterParticleControl>();
            return _instance;
        }
    }
    #endregion

    #region PUBLIC METHODS
    public void PlayParticle(ParticleType type, float playTime)
    {
        if (type == ParticleType.Boost)
            StartCoroutine(PlayRoutine((int)ParticleType.Boost, playTime)); 
    }
    public void PlayParticle(ParticleType type , Transform tr)
    {
        particles[(int)type].transform.position = tr.position;
        particles[(int)type].Play();
    }
    public void StopParticle(ParticleType type)
    {
        particles[(int)type].Stop();
    }
    #endregion

    #region PRIVATE METHODS
    private IEnumerator PlayRoutine(int type, float playTime)
    {
        float timeCounter = 0;
        particles[type].Play();
        while (timeCounter <= 1)
        {
            timeCounter += TimeManager.deltaTime / playTime;
            particles[type].transform.position = this.transform.position;
            yield return null;
        }
        particles[type].Stop();
    }
    #endregion

}
