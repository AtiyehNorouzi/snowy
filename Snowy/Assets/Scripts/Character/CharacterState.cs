﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterState  {

    #region FIELDS
    private static CharacterState _instance;
    private State _characterState = State.NoMove;
    #endregion

    #region ENUMS
    public enum State {NoMove ,  Running, Boost, Dead, Jump }
    #endregion

    #region PROPERTIES
    public static CharacterState Instance
    {
        get
        {
            if (_instance == null)
                _instance = new CharacterState();
            return _instance;
        }
    }
    #endregion

    #region PUBLIC METHODS
	public void ChangeState(State newState)
    {
        _characterState = newState;
    }
    public State GetState()
    {
        return _characterState;
    }
    #endregion
}
