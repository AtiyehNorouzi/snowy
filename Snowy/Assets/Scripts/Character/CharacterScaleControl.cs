﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/*
 * Character Scale Control
 * this script is for managing collisions and scales of the character
 * written by Smokey Shadow
 */
public class CharacterScaleControl : MonoBehaviour {

    #region CONST FIELDS
    const float ENABLE_COLLIDER_TIME = 1f;
    const float DEAD_THRESHOLD = 0.1f;
    const float HINT_THRESHOLD = 0.2f;
    #endregion

    #region FIELDS
    [SerializeField]
    private scaleStateStruct[] _scaleStates; // stone,kid,snowball,machine,jet,kidsnowball

    [SerializeField]
    private float _changeScaleTime;

    [SerializeField]
    private float _boostThreshold;

    [SerializeField]
    private float _boostTimeLength;

    [SerializeField]
    private Sprite _boostSprite;

    private float _boostTimer = 0;

    private float _timer = 0;

    private float _changeTimeLength = 0;

    private Vector3 _newScale = Vector3.zero;

    private bool _changeScale = false;

    private int _currentScaleState = 0;

    private Vector3 _normalScale;

    private SpriteRenderer _sr;

    private Sprite _normalSnowball;
    #endregion

    #region ENUMS
    public enum ScaleState { STONE_D, KID_D, SNOWBALL_U, MACHINE_D, JETPACK_D, KIDSNOW_D }
    #endregion

    #region MONOBEHAVIOUR
    private void Start()
    {
        _normalScale = new Vector3(1,1,0);
        _sr = GetComponent<SpriteRenderer>();
        _normalSnowball = _sr.sprite;
    }

    void Update () {

        if (CharacterState.Instance.GetState() == CharacterState.State.Dead)
            return;

        if (CharacterState.Instance.GetState () == CharacterState.State.Boost) {
            SliderManager.Instance.SetColor(SliderManager.ColorType.Boost);
            _boostTimer += TimeManager.deltaTime  / _boostTimeLength;
            _sr.sprite = _boostSprite;
            if (_boostTimer >= 1) {
                _boostTimer = 0;
                 transform.localScale = _normalScale;
                _sr.sprite = _normalSnowball;
                SliderManager.Instance.SetColor(SliderManager.ColorType.Normal);
                CharacterState.Instance.ChangeState (CharacterState.State.Running);
                return;
            }

        }
        if (transform.localScale.x >= 0.1f && transform.localScale.x <= 0.2f)
            SliderManager.Instance.SetColor(SliderManager.ColorType.Hint);
        else
            SliderManager.Instance.SetColor(SliderManager.ColorType.Normal);
        if (transform.localScale.x >= _boostThreshold && CharacterState.Instance.GetState() != CharacterState.State.Boost)
        {
            CharacterState.Instance.ChangeState (CharacterState.State.Boost);
            CharacterParticleControl.Instance.PlayParticle(CharacterParticleControl.ParticleType.Boost, _boostTimeLength);
        }
        if (CharacterState.Instance.GetState() != CharacterState.State.Dead && transform.localScale.x <= DEAD_THRESHOLD)
        {
            GetComponent<CharacterMovement>().CharacterDeath();
        }

        if (_changeScale) {
            _timer += Time.deltaTime / _changeTimeLength;
           transform.localScale = Vector3.Lerp (transform.localScale, _newScale, _timer);
            if (_timer >= 1)
            {
                _changeScale = false;
                _timer = 0;
            }
        }
     

    }
    private void OnTriggerEnter2D (Collider2D collision) {

        if (collision.gameObject.tag == "snowball") {
            if (CharacterState.Instance.GetState () != CharacterState.State.Boost)
                _currentScaleState = (int) ScaleState.SNOWBALL_U;
        }
        else
            return;
        if (CharacterState.Instance.GetState () != CharacterState.State.Boost)
            ChangeScale ();
        collision.gameObject.GetComponent<Collider2D> ().enabled = false;
        StartCoroutine (EnableCollider (collision.gameObject, ENABLE_COLLIDER_TIME));
    }

    private void OnCollisionEnter2D (Collision2D collision) {

       
         if (collision.gameObject.tag == "machine")
            _currentScaleState = (int) ScaleState.MACHINE_D;
        else if (collision.gameObject.tag == "stone")
            _currentScaleState = (int)ScaleState.STONE_D;
        else if (collision.gameObject.tag == "kid")
            _currentScaleState = (int) ScaleState.KID_D;
        else if (collision.gameObject.tag == "icebullet")
            _currentScaleState = (int) ScaleState.KIDSNOW_D;
        else if (collision.gameObject.tag == "jetpack")
            _currentScaleState = (int) ScaleState.JETPACK_D;
        else
            return;

        if (CharacterState.Instance.GetState () != CharacterState.State.Boost)
            ChangeScale ();
        SoundPlayer.Instance.PlaySound(SoundPlayer.SoundClip.Hit);
        CharacterParticleControl.Instance.PlayParticle(CharacterParticleControl.ParticleType.OnCollideSheild , transform);
        CharacterParticleControl.Instance.PlayParticle(CharacterParticleControl.ParticleType.OnCollideSnow , transform);
        collision.collider.enabled = false;
        StartCoroutine (EnableCollider (collision.gameObject, ENABLE_COLLIDER_TIME));

    }
    #endregion

    #region PRIVATE METHODS
    private void ChangeScale () {
        _newScale = transform.localScale + new Vector3 (_scaleStates[_currentScaleState].ChangeScale, _scaleStates[_currentScaleState].ChangeScale, 0);
        _changeTimeLength = Mathf.Abs (_changeScaleTime * _scaleStates[_currentScaleState].ChangeScale);
        _changeScale = true;
    }
    private IEnumerator EnableCollider (GameObject obj, float delay) {
        yield return new WaitForSeconds (delay);
        obj.GetComponent<Collider2D> ().enabled = true;
    }
    #endregion

    #region STRUCTS
    [System.Serializable]
    private struct scaleStateStruct {
        [SerializeField]
        private ScaleState _downState;

        [SerializeField]
        private float _changeScale;

        public ScaleState DownState {
            get {
                return _downState;
            }
        }
        public float ChangeScale {
            get {
                return _changeScale;
            }
        }
    }
    #endregion
}