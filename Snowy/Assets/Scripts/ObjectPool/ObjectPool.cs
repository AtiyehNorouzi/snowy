﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
/*
 * Written by : Smoky Shadow
 * This script is a Generic ObjectPool 
 * for pooling multiple objects
*/
public class ObjectPool<T> where T : class
{
    public enum ObjType
    {
        Coin = 0, Snowball, Kid  , StoneN1, StoneN2, StoneN3, StoneN4, JetPack, SnowSetN1, SnowSetN2,
        SnowSetN3, SnowSetN4, MountainN1 , MountainN2 , MountainN3 , MountainN4 ,
        MountainN5 , MountainN6 , Machine
    };
    public struct ObjStruct
    {
        public Stack<T> pooledObjs;
        public GameObject objParent;
    }
    public GameObject[] PrefabsWeWantToPool;
    public int[] amounts;
    public ObjStruct[] objWeWantToPool;
    public ObjectPool<T> instance;

    public ObjectPool()
    {
        instance = this;
    }

    public void Init(ObjType type ,  PooledObject [] objects)
    {
        objWeWantToPool = new ObjStruct[objects.Length];
        PrefabsWeWantToPool = new GameObject[objects.Length];
        amounts = new int[objects.Length];
        PrefabsWeWantToPool[(int)type] = objects[(int)type].prefab;
        amounts[(int)type] = objects[(int)type].amount;
        CreatePool(type);
    }

    public void CreatePool(ObjType type)
    {
        int objIndex = (int)type;
        objWeWantToPool[objIndex].pooledObjs = new Stack<T>();
        objWeWantToPool[objIndex].objParent = new GameObject(PrefabsWeWantToPool[objIndex].name + "_parent");
        for (int j = 0; j < amounts[objIndex]; j++)
        {
            GameObject GO = GameObject.Instantiate(PrefabsWeWantToPool[objIndex]);
            T c = typeof(T) == typeof(GameObject) ? GO as T : GO.GetComponent<T>();
            GO.SetActive(false);
            GO.transform.SetParent(objWeWantToPool[objIndex].objParent.transform);
            PushObj(type, c);
        }
    }
    public void PushObj(ObjType type, T obj)
    {
        objWeWantToPool[(int)type].pooledObjs.Push(obj);
        (typeof(T) == typeof(GameObject) ? obj as GameObject : (obj as MonoBehaviour).gameObject).transform.SetParent(objWeWantToPool[(int)type].objParent.transform);
        (typeof(T) == typeof(GameObject) ? obj as GameObject : (obj as MonoBehaviour).gameObject).transform.localPosition = Vector3.zero;
        (typeof(T) == typeof(GameObject) ? obj as GameObject : (obj as MonoBehaviour).gameObject).SetActive(false);
    }

    public T GetObj(ObjType type)
    {
        T p = objWeWantToPool[(int)type].pooledObjs.Pop();
        (typeof(T) == typeof(GameObject) ? p as GameObject : (p as MonoBehaviour).gameObject).SetActive(true);
        (typeof(T) == typeof(GameObject) ? p as GameObject : (p as MonoBehaviour).gameObject).transform.SetParent(null);
        return p;
    }
    public static ObjType GetMountainType(int index)
    {
        return (ObjType)(index + 12);//14 : mountain type offset
    }
    public static ObjType GetSnowType(int index)
    {
        return (ObjType)(index + 8);//8 : snowball type offset
    }
    public static ObjType GetStoneType(int index)
    {
        return (ObjType)(index + 3);//3 : stone type offset
    }

}