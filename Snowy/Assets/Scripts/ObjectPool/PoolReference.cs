﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/*
 * Written by : Smoky Shadow
 * This script is a Reference for
 * Different Pooled Objects
*/
public class PoolReference : MonoBehaviour
{
    public  PooledObject[] objects;
    public static ObjectPool<CoinDrop> coinInstance = new ObjectPool<CoinDrop>();
    public static ObjectPool<ShootingSnow> snowBallInstance = new ObjectPool<ShootingSnow>();
    public static ObjectPool<KidMovement> kidInstance = new ObjectPool<KidMovement>();
    public static ObjectPool<GameObject> stone1Instance = new ObjectPool<GameObject>();
    public static ObjectPool<GameObject> stone2Instance = new ObjectPool<GameObject>();
    public static ObjectPool<GameObject> stone3Instance = new ObjectPool<GameObject>();
    public static ObjectPool<GameObject> stone4Instance = new ObjectPool<GameObject>();
    public static ObjectPool<GameObject> JetPackInstance = new ObjectPool<GameObject>();
    public static ObjectPool<GameObject> snowSet1Instance = new ObjectPool<GameObject>();
    public static ObjectPool<GameObject> snowSet2Instance = new ObjectPool<GameObject>();
    public static ObjectPool<GameObject> snowSet3Instance = new ObjectPool<GameObject>();
    public static ObjectPool<GameObject> snowSet4Instance = new ObjectPool<GameObject>();
    public static ObjectPool<Mountain> mountain1Instance = new ObjectPool<Mountain>();
    public static ObjectPool<Mountain> mountain2Instance = new ObjectPool<Mountain>();
    public static ObjectPool<Mountain> mountain3Instance = new ObjectPool<Mountain>();
    public static ObjectPool<Mountain> mountain4Instance = new ObjectPool<Mountain>();
    public static ObjectPool<Mountain> mountain5Instance = new ObjectPool<Mountain>();
    public static ObjectPool<Mountain> mountain6Instance = new ObjectPool<Mountain>();
    public static ObjectPool<MachineMovement> machineInstance = new ObjectPool<MachineMovement>();
    private void Awake()
    {
        coinInstance.Init(ObjectPool<CoinDrop>.ObjType.Coin , objects);
        snowBallInstance.Init(ObjectPool<ShootingSnow>.ObjType.Snowball, objects);
        kidInstance.Init(ObjectPool<KidMovement>.ObjType.Kid, objects);
        stone1Instance.Init(ObjectPool<GameObject>.ObjType.StoneN1, objects);
        stone2Instance.Init(ObjectPool<GameObject>.ObjType.StoneN2, objects);
        stone3Instance.Init(ObjectPool<GameObject>.ObjType.StoneN3, objects);
        stone4Instance.Init(ObjectPool<GameObject>.ObjType.StoneN4, objects);
        snowSet1Instance.Init(ObjectPool<GameObject>.ObjType.SnowSetN1, objects);
        snowSet2Instance.Init(ObjectPool<GameObject>.ObjType.SnowSetN2, objects);
        snowSet3Instance.Init(ObjectPool<GameObject>.ObjType.SnowSetN3, objects);
        snowSet4Instance.Init(ObjectPool<GameObject>.ObjType.SnowSetN4, objects);
        JetPackInstance.Init(ObjectPool<GameObject>.ObjType.JetPack, objects);
        mountain1Instance.Init(ObjectPool<Mountain>.ObjType.MountainN1, objects);
        mountain2Instance.Init(ObjectPool<Mountain>.ObjType.MountainN2, objects);
        mountain3Instance.Init(ObjectPool<Mountain>.ObjType.MountainN3, objects);
        mountain4Instance.Init(ObjectPool<Mountain>.ObjType.MountainN4, objects);
        mountain5Instance.Init(ObjectPool<Mountain>.ObjType.MountainN5, objects);
        mountain6Instance.Init(ObjectPool<Mountain>.ObjType.MountainN6, objects);
        machineInstance.Init(ObjectPool<MachineMovement>.ObjType.Machine, objects);
    }
    public static ObjectPool<Mountain> GetMountainIsntance(int index)
    {
        return index == 0 ? mountain1Instance : (index == 1 ? mountain2Instance : (index == 2 ? mountain3Instance :
            (index == 3 ? mountain4Instance : (index == 4 ? mountain5Instance : (index == 5 ? mountain6Instance : mountain1Instance)))));
    }
    public static ObjectPool<GameObject> GetRandomSnowSet(out int index)
    {
        index = Random.Range(0, 4);
        return index == 0 ? snowSet1Instance : ((index == 1) ? snowSet2Instance : ((index == 2) ? snowSet3Instance : ((index == 3) ? snowSet4Instance : snowSet1Instance)));
    }
    public static ObjectPool<GameObject> GetRandomStone(out int index)
    {
        index = Random.Range(0, 4);
        return index == 0 ? stone1Instance : index == 1 ? stone2Instance : index == 2 ? stone3Instance : stone4Instance;
    }
    public static ObjectPool<CoinDrop> GetCoinInstance()
    {
        return coinInstance;
    }
    public static ObjectPool<ShootingSnow> GetSnowBulletInstance()
    {
        return snowBallInstance;
    }

}
