﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
/*
this script is for moving kids
*/
public class KidMove : MonoBehaviour {
    #region sources
    //all Kid Cloths
    [Header("cloths")]
    public Sprite [] sprites;
    [Header("for frame frame animating")]
    [Space(5)]
	public float framesPerSecond = 5f;
	private SpriteRenderer spriteRenderer;
    [Header("throw snowball")]
    [Space(5)]
    float ThrowTime;
    bool Throwsnow = true;
    public static float ThrowInterval = 1.5f;
    [Header("----------------------------")]
    [Space(5)]
    public GameObject deadkid;
    public AudioClip jump;
    public GameObject coin;

    #endregion
    // Use this for initialization
    void Start () {
		spriteRenderer = GetComponent<Renderer>() as SpriteRenderer;
        ThrowTime = Time.time;
}
	
	// Update is called once per frame
	void Update () {      
        #region KidMoveAnimate
        int index = (int)(Time.timeSinceLevelLoad * framesPerSecond);
		index = index % sprites.Length;
		spriteRenderer.sprite = sprites[ index ];
        #endregion
        #region whenToThrowSnow
        if (gameObject != null)
        {
            if (GetComponent<Renderer>().isVisible == false)
            {
                Throwsnow = false;
            }
            if (GameObject.FindWithTag("Gole").transform.position.x >= this.transform.position.x)
            {

                Throwsnow = false;
            }
            else
            {
                Throwsnow = true;

            }
        }
        #endregion
        #region shootSnowball
        if (Time.time >= ThrowTime + ThrowInterval && Throwsnow)
        {
                 GameObject obj = ObjectPooler.manager.GetPooledObject("ThrowSnowBall");
                obj.transform.position = new Vector3(this.transform.position.x, this.transform.position.y + 0.5f, this.transform.position.z);
                obj.transform.rotation = this.transform.rotation;
                obj.SetActive(true);
                 ThrowTime = Time.time;
        }
        #endregion

    }
    void OnCollisionEnter2D(Collision2D col){
	
        if(col.gameObject.tag == "Gole")
        {
            #region collideOnHead
            if (GetComponent<EdgeCollider2D>().IsTouching(col.gameObject.GetComponent<CircleCollider2D>()) && col.gameObject.transform.parent.gameObject.transform.localScale.x < 0.25f)
            {
             
                #region CoinDrop
                int random = Random.Range(1, 6);
                for(int i=0;i< random; i++)
                {
                   
                    float speed = Random.Range(2, 5);
                    Vector3 direction = new Vector3(Random.Range(0,3) , Random.RandomRange(0,2) , Random.Range(0,3));
                    GameObject obj= ObjectPooler.manager.GetPooledObject("coin");
                    obj.transform.position = transform.position;
                    obj.transform.rotation = transform.rotation;
                    obj.SetActive(true);
                    obj.GetComponent<Rigidbody2D>().AddForce(direction*speed, ForceMode2D.Impulse);
                    
                }
                #endregion
                SoundPlayer.Instance.PlaySound(SoundPlayer.SoundClip.JumpOnKid);
                col.gameObject.GetComponent<Rigidbody2D>().gravityScale = 1f;
                col.gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector3(0, 1.5f, 0), ForceMode2D.Impulse);
                col.gameObject.GetComponent<Rigidbody2D>().gravityScale = 3.2f;
                gameObject.SetActive(false);
                GameObject dead = (GameObject)Instantiate(deadkid, new Vector3(this.transform.position.x, this.transform.position.y - 0.40f, this.transform.position.z), this.transform.rotation);
                dead.transform.SetParent(this.transform.parent);
                Destroy(dead, 1f);
            }
            #endregion
            #region collideOnbody
            else if (this.GetComponent<PolygonCollider2D>().IsTouching(col.gameObject.GetComponent<CircleCollider2D>()))
            {

       
                        gameObject.GetComponent<PolygonCollider2D>().enabled = false;
                        StartCoroutine(enablecollider(gameObject, 0.5f));

            }
            #endregion

        }
    }
    IEnumerator enablecollider(GameObject obj, float delay)
    {

        yield return new WaitForSeconds(delay);
        obj.GetComponent<PolygonCollider2D>().enabled = true;
  
    }

}
