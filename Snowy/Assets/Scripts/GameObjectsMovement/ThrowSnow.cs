﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/*
script for snowball movement thrown by kid
*/
public class ThrowSnow : MonoBehaviour {
    Vector2 Vector;
    Rigidbody2D rigidbody;
    Transform scale;
    public static bool enable = false;
    void OnEnable()
    {
        rigidbody = this.GetComponent<Rigidbody2D>();
        //if (Character._instance != null)
        //{
        //    if (Character._instance.gameObject.transform.localScale.x >= 1.4)
        //    {
        //        rigidbody.AddForce(new Vector2(-6f, 8.5f), ForceMode2D.Impulse);
        //    }
        //    else
        //    {
        //        rigidbody.AddForce(new Vector2(-6f, 8f), ForceMode2D.Impulse);
        //    }
        //}
   
    }
    void Destroy()
    {
       
        gameObject.SetActive(false);
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "Gole")
        {

            ObjectPooler.manager.pushObject(gameObject, "ThrowSnowBall");
        }
        if (col.gameObject.tag != "snowball")
        {
            ObjectPooler.manager.pushObject(gameObject, "ThrowSnowBall");
        }
      
    }  
    void OnBecameInvisible()
    {
        ObjectPooler.manager.pushObject(gameObject, "ThrowSnowBall");
    }
}
