﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cloudmovement : MonoBehaviour {

    Vector3 movedir;
    public Transform MiddlePosition;
    public Transform StartPosition;
    public Transform EndPosition;
   
	// Use this for initialization
    void OnEnable()
    {
        movedir = (MiddlePosition.position - StartPosition.position).normalized;
    }

	// Update is called once per frame
	void Update () {

        transform.position += movedir.normalized * 5 * TimeManager.deltaTime;
       
        if (transform.position.x <= EndPosition.position.x)
        {
            Debug.Log("invisible" + gameObject.name);
            ObjectPooler.manager.pushObject(gameObject, "Cloud");
        }
   }
}
