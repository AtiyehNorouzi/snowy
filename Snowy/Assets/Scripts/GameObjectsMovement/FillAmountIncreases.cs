﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class FillAmountIncreases : MonoBehaviour {

    #region CONST FIELDS
    private const float CHARACTER_MIN_SCALE = 0.2f;
    private const float CHARACTER_MAX_SCALE = 2f;
    #endregion

    #region FIELDS
    [SerializeField]
    private GameObject _character;

    [SerializeField]
    private Image _image;
    #endregion

    #region MONO BEHAVIOUR
    private void Update()
    {
        _image.fillAmount = MapValues(_character.transform.localScale.x, CHARACTER_MIN_SCALE, CHARACTER_MAX_SCALE, 0f , 1);
    }
    #endregion

    #region PRIVATE METHODS
    private float MapValues(float currentPercent , float inMin , float inMax , float outMin , float outMax)
    {
        return (currentPercent - inMin) * (outMax - outMin) / (inMax - inMin) + outMin;
    }
    #endregion

}
