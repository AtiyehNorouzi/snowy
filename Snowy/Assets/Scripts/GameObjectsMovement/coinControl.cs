﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/*
script for Coins Colliding
*/
public class coinControl : MonoBehaviour {
    GameObject parent;
    void Start()
    {
        parent = GameObject.Find("ObjectPooler").transform.GetChild(0).gameObject;
    }
    void OnCollisionEnter2D(Collision2D col)
    {
        if(col.gameObject.tag == "mountain")
        {
            Invoke("invisible", 2f);
            Invoke("Timer", 0.5f);
            transform.SetParent(col.gameObject.transform);
        }
        if(col.gameObject.tag == "Gole")
        {
            Invoke("Invisible" , 0);
        }
    }

    void invisible()
    {
        gameObject.transform.SetParent(parent.transform);
        ObjectPooler.manager.pushObject(gameObject, "coin");
    }
    void Timer()
    {
        GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezePositionX;
    }
}
