﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/*
 * Written by : Smoky Shadow
 * This script is for sticking snowballs to character when they are in a specific distance
*/
public class SnowBallJoin : MonoBehaviour {

    #region CONST FIELDS
    private const float DIST = 1f;

    private const float VEL_MULTIPLIER = 6f;
    #endregion

    #region FIELDS
    private CharacterMovement _gole;

    private bool _stick = false;

    private Vector3 _dir;
    #endregion

    #region MONO BEHAVIOUR

    void Start () {
        _gole = GameObject.FindWithTag("Gole").GetComponent<CharacterMovement>();
	}

	void Update () {

        if(_stick)
        {
            _dir = _gole.gameObject.transform.position - transform.position;
            transform.Translate(_dir.normalized * _gole.GetVelocity() * VEL_MULTIPLIER * TimeManager.deltaTime);
        }

        if (Vector2.Distance(_gole.gameObject.transform.position, transform.position) <= DIST && _stick == false)
            _stick = true;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Gole")
        {
            gameObject.SetActive(false);
        }
    }
    

    #endregion
}
