﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
/*
 * Written by : Smoky Shadow
 * This script is for Character movements and behaviours
*/
public class Character : MonoBehaviour
{
    public enum State
    {
        NORMAL, BOOT, NOMOVE
    }

    const float scaleTime = 1f;
    const float rotateDegree = -360;

   	void Update () {
        
        transform.RotateAround(transform.position, Vector3.forward, rotateDegree * TimeManager.deltaTime * SceneControl.Instance.GetCurrentVelocity());

    }
   
    

    void OnTriggerEnter2D(Collider2D col){
        if(col.tag == "snowball")
        {
            Debug.Log("trigger");
            ScaleSnowBall(0.5f, increase: true);
        }

    }


    void OnCollisionEnter2D(Collision2D col) {
   

    }


    public void ScaleSnowBall(float scaleRate , bool increase)
    {
        StartCoroutine(ScaleRoutine(transform.localScale + (increase ? +1 : -1 )*transform.localScale*scaleRate));
    }
    IEnumerator ScaleRoutine(Vector3 targetScale)
    {
        Vector3 startScale = transform.localScale;
        float timer = 0;
        while(true)
        {
            timer += TimeManager.deltaTime / scaleTime;
            if (!Utility.AlmostEqual(transform.localScale, targetScale, 0.05f))
                transform.localScale = Vector3.Lerp(startScale, targetScale, timer);
            else
                yield break;
            yield return null;
        }

    }
     
    void jump()
    {
   
    }

}