﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/*
script for machine Kid Movement
movement desciption:
move to the mountain path more faster than the mountain
*/
public class machineMove : MonoBehaviour {
 
    //layer mask for define what to hit
    public LayerMask whatToHit;
    //speed of the machine 
    float speed;
    void Start()
    {
        //set the machine speed on start , * 2 scale of mountain velocity
     //   speed = SceneControl.manager.velocityNow * 1.2f;
    }
	// Update is called once per frame
	void Update () {
        RaycastHit2D rch;
        rch = Physics2D.Raycast(transform.position + Vector3.up * 3, Vector3.down, 6, whatToHit);
        if (rch.collider != null)
        {
            transform.position = rch.point + 0.5f * Vector2.up;
        }
        rch = Physics2D.Raycast(transform.position + Vector3.up * 3 + Vector3.right, Vector3.down, 6, whatToHit);
        Vector2 NoqteJolo = rch.point;
        rch = Physics2D.Raycast(transform.position + Vector3.up * 3 - Vector3.right, Vector3.down, 6, whatToHit);
        Vector2 NoqteAghab = rch.point;
        Vector2 direction = NoqteJolo - NoqteAghab;
        var angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
        transform.Translate(Vector3.left * speed * Time.deltaTime, Space.World);

    }
    

}
