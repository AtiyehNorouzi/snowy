﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/*script for moving the jetpack
movement:come to first position from out the page , 
come to catch gole , move to top and ome back to first position (do it 2 times)
if gole jump on the head move the gole up to clouds else collide it and move to left
*/
public class JetPackMove : MonoBehaviour {
    #region sources
    //position
    public Transform golePosition;
    public Transform endposition;
    public Transform onCloud;
    Vector3 startposition;
    LTSpline cr;
    int count = 0;
    bool waitTime = true;
    public static bool headCollie = false;
    public static bool bodyCollide = false;
    public static bool geton = false;
    public static bool keyboardlock = false;
    public static bool finishedMove = false;
    void OnEnable()
    {
        headCollie = false;
        bodyCollide = false;
        geton = false;
        keyboardlock = false;
        finishedMove = false;
    }
    #endregion
    void Start()
    {
        //initialize the position
        startposition = transform.position;
        onCloud = GameObject.Find("OnCloud").transform;
        endposition = GameObject.Find("intheAir").transform;
        golePosition = GameObject.Find("gooleeeee").transform.parent.gameObject.transform;
    }
    void Update()
    {
        #region JetpackMove 
        //when gole not collide on jetpack
        if (!headCollie && !bodyCollide)
        {
            //get into endPosition
            if (AlmostEqual(transform.position, endposition.position, 0.1f))
            {
                if (!waitTime)
                {
                    count++;
                    //if moving repeats three times , move to left
                    if (count == 3)
                    {
                        Vector3[] next = new Vector3[] { Vector3.zero, endposition.position, new Vector3(endposition.position.x - 20f, endposition.position.y, endposition.position.z), Vector3.zero };
                        LeanTween.moveSpline(gameObject, next, 5f).setEase(LeanTweenType.easeOutBack);
                    }
                    else
                    {
                        //set flipx and fire direction
                        golePosition = GameObject.Find("gooleeeee").transform.parent.gameObject.transform;
                        GetComponent<SpriteRenderer>().flipX = true;
                        transform.GetChild(0).gameObject.SetActiveRecursively(false);
                        transform.GetChild(1).gameObject.SetActiveRecursively(false);
                        transform.GetChild(2).gameObject.SetActiveRecursively(true);
                        transform.GetChild(3).gameObject.SetActiveRecursively(true);
                        cr = new LTSpline(new Vector3[] { Vector3.zero, endposition.position, golePosition.position, startposition, Vector3.zero });
                        LeanTween.move(gameObject, cr, 5f).setOrientToPath2d(false).setEase(LeanTweenType.easeOutQuad);
                    }
                    waitTime = true;
                }
            }
            //get into startPosition
            else if (AlmostEqual(transform.position, startposition, 0.1f))
            {
                if (waitTime)
                {
                    golePosition = GameObject.Find("gooleeeee").transform.parent.gameObject.transform;
                    GetComponent<SpriteRenderer>().flipX = false;
                    transform.GetChild(0).gameObject.SetActiveRecursively(true);
                    transform.GetChild(1).gameObject.SetActiveRecursively(true);
                    transform.GetChild(2).gameObject.SetActiveRecursively(false);
                    transform.GetChild(3).gameObject.SetActiveRecursively(false);
                    cr = new LTSpline(new Vector3[] { Vector3.zero, startposition, golePosition.position, endposition.position, Vector3.zero });
                    LeanTween.move(gameObject, cr, 5f).setOrientToPath2d(false).setEase(LeanTweenType.easeOutQuad);
                    waitTime = false;
                }
            }

        }
        #endregion
        //when hit body and head
        else
        {
        
            //when jetpack get to the cloud position
            if (AlmostEqual(transform.position , onCloud.position , 0.1f))
            {
                keyboardlock = true;
                GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;

            }
  
            if (keyboardlock)
            {
                float speed = 7f;
                if (Input.GetKey(KeyCode.S))
                {
                    transform.Translate(-Vector3.up * speed * Time.deltaTime);
                }
                else if (Input.GetKey(KeyCode.W))
                {
                    transform.Translate(Vector3.up * speed * Time.deltaTime);
                }
            }
            else
            { //when jump on the jetpack, jetpack will move to the cloud position
                if (geton)
                {
                 transform.position = Vector3.MoveTowards(transform.position, onCloud.position, Time.deltaTime * 5f);
                    if(GetComponent<SpriteRenderer>().flipX == false)
                    {
                        GetComponent<SpriteRenderer>().flipX = true;
                        transform.GetChild(0).gameObject.SetActiveRecursively(false);
                        transform.GetChild(1).gameObject.SetActiveRecursively(false);
                        transform.GetChild(2).gameObject.SetActiveRecursively(true);
                        transform.GetChild(3).gameObject.SetActiveRecursively(true);
                    }
                }
                //if it collide on the body move out to left side of the screen
                else
                {
                     transform.Translate(Vector3.left * Time.deltaTime * 5f);

                }
            }
   
        }
        if (finishedMove)
        {
            golePosition.SetParent(null);
            golePosition.GetChild(0).gameObject.GetComponent<Animator>().enabled = true;
            golePosition.localScale = JetPackHea.tempScale;
            GetComponent<FixedJoint2D>().enabled = false;
            GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Kinematic;
            transform.Translate(Vector3.right * Time.deltaTime * 5f);

        }
    }

    //function for  check equality of two vectors with a precision errs
    public static bool AlmostEqual(Vector3 v1, Vector3 v2, float precision)
    {
        bool equal = true;

        if (Mathf.Abs(v1.x - v2.x) > precision) equal = false;
        if (Mathf.Abs(v1.y - v2.y) > precision) equal = false;


        return equal;
    }

  
    void OnBecameInvisible()
    {
        if (finishedMove){
             Destroy(this.gameObject);
        }
        }
}
