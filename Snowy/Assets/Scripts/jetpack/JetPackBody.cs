﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/*
script for colliding on jetpackHead
*/
public class JetPackBody : MonoBehaviour {

    //decrease the snow ball health here
    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.tag == "Gole" && !JetPackMove.headCollie)
        {
           
      

            JetPackMove.bodyCollide = true;
            gameObject.GetComponent<Collider2D>().enabled = false;
            StartCoroutine(enablecollider(gameObject ,  0.5f));
            JetPackMove.geton = false;


        }
    }
    public IEnumerator enablecollider(GameObject obj, float delay)
    {
        yield return new WaitForSeconds(delay);
        obj.GetComponent<Collider2D>().enabled = true;
    }
}
