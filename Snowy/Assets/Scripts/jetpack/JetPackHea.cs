﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/*
script for colliding on jetpack 's head
*/
public class JetPackHea : MonoBehaviour {

    float usedScale;
    public static Vector3 tempScale;
    void OnCollisionEnter2D(Collision2D col)
    {

        if (col.gameObject.tag == "Gole" && !JetPackMove.bodyCollide)
        {

            JetPackMove.headCollie = true;
            JetPackMove.geton = true;
            col.gameObject.GetComponent<Animator>().enabled = false;
            col.gameObject.transform.rotation = Quaternion.Euler(0, 0, 0);
            var emptyObject = gameObject.transform.parent.gameObject.transform.Find("GolePosition") ;
            emptyObject.transform.SetParent(transform.parent);
            tempScale = col.gameObject.transform.parent.gameObject.transform.localScale;
            usedScale = getNewScale(col.gameObject.transform.parent.gameObject.transform.localScale.x , transform.parent.gameObject.transform.localScale.x);
            col.transform.parent.gameObject.transform.SetParent(emptyObject.transform);
            col.transform.parent.gameObject.transform.localScale = new Vector3(usedScale ,usedScale ,0);
            col.transform.localScale = new Vector3(1, 1, 0);
            col.transform.parent.gameObject.transform.localPosition = Vector3.zero;
            col.transform.rotation = Quaternion.identity;
            col.gameObject.transform.localPosition = Vector3.zero;
            gameObject.layer = LayerMask.NameToLayer("jetpackHead");
            var joint = gameObject.transform.parent.gameObject.AddComponent<FixedJoint2D>();
            joint.connectedBody = col.transform.GetComponent<Rigidbody2D>();

        }
    }
     float getNewScale(float s1,float s)
    {
        return  s1 + (s1 - (s1* s))/s;
    }
}
