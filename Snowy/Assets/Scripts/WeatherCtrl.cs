﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaderCtrl : MonoBehaviour
{
    public ParticleSystem ptr;
    public ParticleSystem rain;
    public ParticleSystem mist;
    public AudioSource rainSource;
    float timeforsnow;
    int randomTime;
    int TimeInterval;
    // Start is called before the first frame update
    void Start()
    {
        randomTime = Random.Range(0, 2);
        timeforsnow = Time.time;
    }

    // Update is called once per frame
    void Update()
    {
        #region weather change

        //time for snow
        if (randomTime == 1)
        {
            if (Time.time >= timeforsnow + TimeInterval)
            {
                TimeInterval = Random.Range(10, 20);
                timeforsnow = Time.time;
                int which = Random.Range(0, 3);

                if (which == 0)
                {
                    if (rain.isPlaying)
                    {
                        rain.Stop();
                        mist.Stop();
                    }
                    ptr.Play();
                }
                else if (which == 1)
                {
                    ptr.Stop();
                    rain.Play();
                    mist.Play();
                    rainSource.Play(1);
                }
                else
                {
                    rain.Stop();
                    mist.Stop();
                    ptr.Stop();
                }

            }
        }
        else
        {
            TimeInterval = Random.Range(10, 15);
            randomTime = 1;

        }
        #endregion

    }
}
