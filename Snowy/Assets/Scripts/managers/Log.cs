﻿using UnityEngine;
using UnityEngine.UI;
public class Log : MonoBehaviour
{
  
    private Text log_text;
    float deltaTime;

    private void Start()
    {
        log_text = GetComponent<Text>();
    }
    void Update()
    {
     
        deltaTime += (Time.unscaledDeltaTime - deltaTime) * 0.1f;
        float msec = deltaTime * 1000.0f;
        float fps = 1.0f / deltaTime;
        string text = string.Format("{0:0.0} ms ({1:0.} fps)", msec, fps);
        log_text.text = text;
    }

   
}
