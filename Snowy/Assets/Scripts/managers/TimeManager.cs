﻿using UnityEngine;
using System.Collections;
/*
    this script is for managing time an when the time.timescale set to zero all time.deltatime we set in all script would be disabled instantely.
*/
public class TimeManager : MonoBehaviour
{
    #region STATIC FILELDS
    public static float timeScale = 1;
    #endregion

    #region PROPERTIES
    public static float deltaTime
    {
        get
        {
            return Time.deltaTime * timeScale;
        }
    }
    #endregion

}
