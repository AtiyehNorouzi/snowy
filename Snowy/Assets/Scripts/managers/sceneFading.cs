﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//the script for changing the background Color 
public class sceneFading : MonoBehaviour {
    public bool fadeIn = true;
   public  static float minimum = 0.4f;
    static float maximum = 1f;
    static float duration=50f;
    static float startTime;
    public static Color color;
    public static float timer;
	// Use this for initialization
	void Awake () {
        color = new Color(1f, 1f, 1f, minimum);
        startTime = Time.time;
	}

    // Update is called once per frame
    void Update() {
    
   
    }
    public static Color getcolor()
    {
        float t = (timer - startTime) / duration;
        color = new Color(1f, 1f, 1f, Mathf.SmoothStep(minimum, maximum, t));
        return color;
    }
}
