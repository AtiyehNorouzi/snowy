﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameAssetRefs : MonoBehaviour
{
    #region STATIC FIELDS
    private static GameAssetRefs _instance;
    #endregion

    #region FIELDS
    [SerializeField]
    private List<KidCloth> _kidCloths;

    [SerializeField]
    private List<Sprite> _machineColor;

    [SerializeField]
    private List<Sprite> _waterSprites;
    #endregion

    #region PROPERTIES
    public static GameAssetRefs Instance
    {
        get
        {
            if (!_instance)
                _instance = GameObject.FindObjectOfType<GameAssetRefs>();
            return _instance;
        }
    }


    public  List<KidCloth> KidCloths
    {
        get
        {
            return _kidCloths;
        }
    }

    public List<Sprite> MachineColors
    {
        get
        {
            return _machineColor;
        }

    }

    public List<Sprite> WaterSprites
    {
        get
        {
            return _waterSprites;
        }

    }
    #endregion

    #region STRUCTS
    [System.Serializable]
    public struct KidCloth
    {
        public int type;
        public List<Sprite> walkAnimation;
        public List<Sprite> deadAnimation;
    }
    #endregion
}
