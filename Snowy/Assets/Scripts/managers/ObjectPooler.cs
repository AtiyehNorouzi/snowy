﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPooler : MonoBehaviour
{
    [System.Serializable]
    public struct GameObjectStruct
    {
        public string name;
        public int amount;
        public GameObject prefab;
        public Stack<GameObject> pooledGameObjects;
        public int amountNow;
    }
    public GameObjectStruct[] ObjectsWeWantToPool;
    public static ObjectPooler manager;
    void Awake()
    {
        manager = this;
        CreatePooledObjs();

    }
    /*the structure for all Pooled Of Different GameObjects we want to Create on the Load Time,
     *  before the Main Contain of the Game Begin.*/

    public void CreatePooledObjs()
    {
        if (!manager)
        {
            Debug.LogError("you are called createInstance but pooler variable is null");
        }

        for (int i = 0; i < ObjectsWeWantToPool.Length; i++)
        {
            ObjectsWeWantToPool[i].pooledGameObjects = new Stack<GameObject>();
            ObjectsWeWantToPool[i].amountNow = ObjectsWeWantToPool[i].amount;
            //create a new GameObject for grouping each PooledGameObjects
            GameObject ParentPooledGameObject = new GameObject(ObjectsWeWantToPool[i].name + "_Parent");
            ParentPooledGameObject.transform.parent = manager.gameObject.transform;
            for (int j = 0; j < ObjectsWeWantToPool[i].amount; j++)
            {
                Spawn(i, j, ParentPooledGameObject);
            }
        }

    }

    void Spawn(int i, int j, GameObject Parent)
    {
        if (ObjectsWeWantToPool[i].prefab == null)
        {
            Debug.LogError(ObjectsWeWantToPool[i].name + "Prefab NULL!");
        }
        var GO = Instantiate(ObjectsWeWantToPool[i].prefab);
        GO.SetActive(false);
        GO.name = ObjectsWeWantToPool[i].name + "_" + j;
        GO.transform.parent = Parent.transform;
        ObjectsWeWantToPool[i].pooledGameObjects.Push(GO);

    }
    public void pushObject(GameObject pushedObj , string name)
    {
        if (name.Equals("coin"))
        {
            ObjectsWeWantToPool[0].pooledGameObjects.Push(pushedObj);
            pushedObj.SetActive(false);
        }
        else if (name.Equals("ThrowSnowBall"))
        {
            ObjectsWeWantToPool[1].pooledGameObjects.Push(pushedObj);
            pushedObj.SetActive(false);
        }
        else if (name.Equals("Kid"))
        {
            ObjectsWeWantToPool[2].pooledGameObjects.Push(pushedObj);
            pushedObj.SetActive(false);
        }
        else if (name.Equals("Stone1"))
        {
            ObjectsWeWantToPool[3].pooledGameObjects.Push(pushedObj);
            pushedObj.SetActive(false);
        }
        else if (name.Equals("Stone2"))
        {
            ObjectsWeWantToPool[4].pooledGameObjects.Push(pushedObj);
            pushedObj.SetActive(false);
        }
        else if (name.Equals("Stone3"))
        {
            ObjectsWeWantToPool[5].pooledGameObjects.Push(pushedObj);
            pushedObj.SetActive(false);
        }
        else if (name.Equals("Stone4"))
        {
            ObjectsWeWantToPool[6].pooledGameObjects.Push(pushedObj);
            pushedObj.SetActive(false);
        }
        else if (name.Equals("JetPack"))
        {
            ObjectsWeWantToPool[7].pooledGameObjects.Push(pushedObj);
            pushedObj.SetActive(false);
        }
        else if (name.Equals("SnowBall3"))
        {
            ObjectsWeWantToPool[8].pooledGameObjects.Push(pushedObj);
            pushedObj.SetActive(false);
        }
        else if (name.Equals("snowBall4Cube"))
        {
            ObjectsWeWantToPool[9].pooledGameObjects.Push(pushedObj);
            pushedObj.SetActive(false);
        }
        else if (name.Equals("snowBall4Normal"))
        {
            ObjectsWeWantToPool[10].pooledGameObjects.Push(pushedObj);
            pushedObj.SetActive(false);
        }
        else if (name.Equals("snowBall5"))
        {
            ObjectsWeWantToPool[11].pooledGameObjects.Push(pushedObj);
            pushedObj.SetActive(false);
        }
        else if (name.Equals("Cloud"))
        {
            ObjectsWeWantToPool[12].pooledGameObjects.Push(pushedObj);
            pushedObj.SetActive(false);
        }
        else if (name.Equals("metalTigh"))
        {
            ObjectsWeWantToPool[13].pooledGameObjects.Push(pushedObj);
            pushedObj.SetActive(false);
        }
        else if (name.Equals("FloatingEnemy"))
        {
            ObjectsWeWantToPool[14].pooledGameObjects.Push(pushedObj);
            pushedObj.SetActive(false);
        }
        else if (name.Equals("Kid2"))
        {
            ObjectsWeWantToPool[15].pooledGameObjects.Push(pushedObj);
            pushedObj.SetActive(false);
        }
        else if (name.Equals("Kid3"))
        {
            ObjectsWeWantToPool[16].pooledGameObjects.Push(pushedObj);
            pushedObj.SetActive(false);
        }
        else if (name.Equals("Kid4"))
        {
            ObjectsWeWantToPool[17].pooledGameObjects.Push(pushedObj);
            pushedObj.SetActive(false);
        }
        else if (name.Equals("onaircoin"))
        {
            ObjectsWeWantToPool[18].pooledGameObjects.Push(pushedObj);
            pushedObj.SetActive(false);
        }

    }
    public GameObject GetPooledObject(string name)
    {
        if (name.Equals("coin"))
        {
            GameObject obj = ObjectsWeWantToPool[0].pooledGameObjects.Pop();
            obj.SetActive(true);
            return obj;
        }
        else if(name.Equals("ThrowSnowBall"))
        {
            GameObject obj = ObjectsWeWantToPool[1].pooledGameObjects.Pop();
            obj.SetActive(true);
            return obj;
        }
        else if (name.Equals("Kid"))
        {
            GameObject obj = ObjectsWeWantToPool[2].pooledGameObjects.Pop();
            obj.SetActive(true);
            return obj;
        }
        else if (name.Equals("Stone1"))
        {
            GameObject obj = ObjectsWeWantToPool[3].pooledGameObjects.Pop();
            obj.SetActive(true);
            return obj;
        }
        else if (name.Equals("Stone2"))
        {
            GameObject obj = ObjectsWeWantToPool[4].pooledGameObjects.Pop();
            obj.SetActive(true);
            return obj;
        }
        else if (name.Equals("Stone3"))
        {
            GameObject obj = ObjectsWeWantToPool[5].pooledGameObjects.Pop();
            obj.SetActive(true);
            return obj;
        }
        else if (name.Equals("Stone4"))
        {
            GameObject obj = ObjectsWeWantToPool[6].pooledGameObjects.Pop();
            obj.SetActive(true);
            return obj;
        }
        else if (name.Equals("JetPack"))
        {
            GameObject obj = ObjectsWeWantToPool[7].pooledGameObjects.Pop();
            obj.SetActive(true);
            return obj;
        }
        else if (name.Equals("SnowBall3"))
        {
            GameObject obj = ObjectsWeWantToPool[8].pooledGameObjects.Pop();
            obj.SetActive(true);
            return obj;
        }
        else if (name.Equals("snowBall4Cube"))
        {
            GameObject obj = ObjectsWeWantToPool[9].pooledGameObjects.Pop();
            obj.SetActive(true);
            return obj;
        }
        else if (name.Equals("snowBall4Normal"))
        {
            GameObject obj = ObjectsWeWantToPool[10].pooledGameObjects.Pop();
            obj.SetActive(true);
            return obj;
        }
        else if (name.Equals("snowBall5"))
        {
            GameObject obj = ObjectsWeWantToPool[11].pooledGameObjects.Pop();
            obj.SetActive(true);
            return obj;
        }
        else if (name.Equals("Cloud"))
        {
            GameObject obj = ObjectsWeWantToPool[12].pooledGameObjects.Pop();
            obj.SetActive(true);
            return obj;
        }
        else if (name.Equals("metalTigh"))
        {
            GameObject obj = ObjectsWeWantToPool[13].pooledGameObjects.Pop();
            obj.SetActive(true);
            return obj;
        }
        else if (name.Equals("FloatingEnemy"))
        {
            GameObject obj = ObjectsWeWantToPool[14].pooledGameObjects.Pop();
            obj.SetActive(true);
            return obj;
        }
        else if (name.Equals("Kid2"))
        {
            GameObject obj = ObjectsWeWantToPool[15].pooledGameObjects.Pop();
            obj.SetActive(true);
            return obj;
        }
        else if (name.Equals("Kid3"))
        {
            GameObject obj = ObjectsWeWantToPool[16].pooledGameObjects.Pop();
            obj.SetActive(true);
            return obj;
        }
        else if (name.Equals("Kid4"))
        {
            GameObject obj = ObjectsWeWantToPool[17].pooledGameObjects.Pop();
            obj.SetActive(true);
            return obj;
        }
        else if (name.Equals("onaircoin"))
        {
            GameObject obj = ObjectsWeWantToPool[18].pooledGameObjects.Pop();
            return obj;
        }
        return null;
    }

}
