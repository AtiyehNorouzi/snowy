﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

/* 
script for managing the menu pages
*/
public class Scene_Controller : MonoBehaviour
{
    #region FIELDS
    [Header("Scene attributes")]
    [SerializeField]
    private GameObject[] _disableOnSceneChange;
    [SerializeField]
    private Camera _camera;

    private float _sceneChangeAlpha;
    private bool _onChangingScene;
    private GameObject _currentPanel;
    private GameObject _pendingState;

    [Header("states prefabs")]
    [Space(5)]
    [SerializeField]
    private GameObject _exit;
    [SerializeField]
    private GameObject _menu;
    [SerializeField]
    private GameObject _byePage;
    [SerializeField]
    private GameObject _aboutUs;
    [SerializeField]
    private GameObject _comic;
    #endregion

    #region MONO BEHAVIOURS
    void Start()
    {
        _currentPanel = _menu;
    }
    void Update()
    {
        UpdateSceneFading();
    }
    #endregion

    #region PUBLIC METHODS
    public void Exit()
    {
        _sceneChangeAlpha = _disableOnSceneChange[2].GetComponent<SpriteRenderer>().color.a;
        _pendingState = _currentPanel;
        _currentPanel = _exit;
        _onChangingScene = true;
        _menu.SetActive(false);
    }

    public void BackToMenu()
    {

        _sceneChangeAlpha = _currentPanel.GetComponent<Image>().color.a;
        _pendingState = _currentPanel;
        _currentPanel = _menu;
        _onChangingScene = true;
        _menu.SetActive(true);

    }

    public void QuitGame()
    {
        _sceneChangeAlpha = 1;
        _pendingState = _exit;
        _currentPanel = _byePage;
        _onChangingScene = true;

    }
    public void Story()
    {
        _sceneChangeAlpha = _disableOnSceneChange[2].GetComponent<SpriteRenderer>().color.a;
        _pendingState = _currentPanel;
        _currentPanel = _comic;
        _onChangingScene = true;
        _menu.SetActive(false);
    }
    public void AboutUs()
    {

        _sceneChangeAlpha = _disableOnSceneChange[2].GetComponent<SpriteRenderer>().color.a;
        _pendingState = _currentPanel;
        _currentPanel = _aboutUs;
        _onChangingScene = true;
        _menu.SetActive(false);

    }

    public void Retry()
    {
        SceneManager.LoadScene("snowball", LoadSceneMode.Single);

    }
    #endregion

    #region PRIVATE METHODS
    private void UpdateSceneFading()
    {
        //if the button clicked and changing to another scene
        if (_onChangingScene)
        {
            //if the currentscene isnt transparent
            if (_sceneChangeAlpha > 0 )
            {
                if (_pendingState != _menu)
                {     
                    _sceneChangeAlpha = Mathf.Lerp(_sceneChangeAlpha, 0, TimeManager.deltaTime * 1 / _sceneChangeAlpha);
                    _pendingState.GetComponent<Image>().color = new Color(1, 1, 1, _sceneChangeAlpha);
                } 
                else
                {
                    _sceneChangeAlpha = Mathf.Lerp(_sceneChangeAlpha, 0, TimeManager.deltaTime * 1 / _sceneChangeAlpha);
                    SetGray();
                }

            }
            //if the alpha is zero
            else
            {
                if (_pendingState != _menu)
                {
                    _sceneChangeAlpha = 0;
                    _currentPanel.SetActive(true);
                    _pendingState.SetActive(false);
                    _pendingState.GetComponent<Image>().color = new Color(1, 1, 1, 1);
                    SetActiveAnimators(true);
                    _onChangingScene = false;

                }
                else
                {
                    _sceneChangeAlpha = 0;
                    _currentPanel.SetActive(true);
                    _disableOnSceneChange[2].GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 1);
                    SetActiveAnimators(false);
                    _onChangingScene = false;
                }
            }
        }
    }


    private void SetGray()
    {
        for (int i = 0; i < _disableOnSceneChange.Length - 1; i++)
        {
            _disableOnSceneChange[i].GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, _sceneChangeAlpha);
        }
    }

    private void SetActiveAnimators(bool able)
    {

        for (int i = 0; i < _disableOnSceneChange.Length; i++)
        {
            if (i != 2)
            {
                _disableOnSceneChange[i].SetActive(able);

                if (_disableOnSceneChange[i].GetComponent<SpriteRenderer>() != null)
                {
                    _disableOnSceneChange[i].GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 1);
                }
            }
        }

    }
    #endregion



}
