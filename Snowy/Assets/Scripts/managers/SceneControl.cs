﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/*
 * Written by : Smoky Shadow
 * This script is for changing levels
 * and spawning enemies 
*/
public class SceneControl : MonoBehaviour
{
    #region CONST FIELDS
    private const float EACH_LEVEL_TIME = 5f;
    #endregion

    #region FIELDS
    private static SceneControl _instance;

    [SerializeField]
    private Level[] _levels;

    [SerializeField]
    private Transform _startTransform;

    [SerializeField]
    private Transform _endPosition;

    [SerializeField]
    private List<MountainSprite> MountainSprites;

    [SerializeField]
    private int _firstNumOfMountain;

    [SerializeField]
    private int _currentLevel = 0;

    private List<Mountain> _mountainQueue;

    private Vector3 _mountainDir;

    private float _movingVelocity;

    private int _waveCounter = 2;

    private float _speedTimer = 0;

    private ObjectPool<Mountain> _mountianPoolInstance;
    #endregion

    #region PROPERTIES
    public static SceneControl Instance
    {
        get
        {
            if (_instance != null)
                _instance = GameObject.FindObjectOfType<SceneControl>();
            return _instance;
        }
    }
    #endregion

    #region MONO BEHAVIOURS
    private void Start()
    {
        _instance = this;
        _mountainQueue = new List<Mountain>(_firstNumOfMountain);
        _movingVelocity = _levels[_currentLevel].Velocity;
        _mountainDir = (_endPosition.position - _startTransform.position);
        StartCoroutine(OnUpdateScene());
        for (int i=0; i<_firstNumOfMountain; i++)
        {
            _mountainQueue.Add(SpawnNewMountain(pos : _startTransform.position - (i * _mountainDir)));
        }
   
    }
    #endregion

    #region PUBLIC METHODS
    public float GetCurrentVelocity()
    {
        return _movingVelocity;
    }

    public int GetCurrentLevel()
    {
        return _currentLevel;
    }
    #endregion

    #region PRIVATE METHODS
    private IEnumerator OnUpdateScene()
    {
        while(true)
        {
            if (_waveCounter >= _levels[_currentLevel].WaveNumbers)
            {
                _waveCounter = 0;
                _currentLevel++;
            }
            if (CharacterState.Instance.GetState() != CharacterState.State.NoMove)
            {
                #region VELOCITY CONTROL
                _speedTimer += TimeManager.deltaTime / (_levels[_currentLevel].WaveNumbers * EACH_LEVEL_TIME);
                if (_currentLevel + 1 < _levels.Length)
                    _movingVelocity = Mathf.Lerp(_levels[_currentLevel].Velocity,
                    _levels[_currentLevel + 1].Velocity, _speedTimer);
                #endregion

                #region  MOUNTAIN SPAWN
                if(!_mountainQueue[0].SpRenderer.isVisible)
                {
                    _mountainQueue[0].Despawn();
                    ShiftAllMountains();
                }
                #endregion

            }
            yield return null;
        }
    }
    private void ShiftAllMountains()
    {
    
        for (int i = 0; i < _firstNumOfMountain; i++)
        {
     
            if (i == _firstNumOfMountain - 1)
            {
                _mountainQueue[_firstNumOfMountain - 1] = SpawnNewMountain(pos : _mountainQueue[_firstNumOfMountain - 2].transform.position -  _mountainDir );
            }
            else
                _mountainQueue[i] = _mountainQueue[i + 1];
        }
     
    }

    private Mountain SpawnNewMountain(Vector3 pos)
    {
        //dont want to spawn mountains with hole when character is on boost state
        int random = CharacterState.Instance.GetState() == CharacterState.State.Boost ? Random.Range(0, 4):
            Random.Range(0, MountainSprites.Count);
        _mountianPoolInstance = PoolReference.GetMountainIsntance(random);
        Mountain mountain =  _mountianPoolInstance.GetObj(ObjectPool<Mountain>.GetMountainType(random));
        mountain.transform.localPosition = pos;
        mountain.SetSprite(MountainSprites[random].sprites[Random.Range(0, MountainSprites[random].sprites.Count)]);
        mountain.CreateMountain(Random.Range(_levels[_currentLevel].minEnemies, _levels[_currentLevel].maxEnemies), _levels[_currentLevel].hasStone,
                _levels[_currentLevel].hasKid, _levels[_currentLevel].hasMachine, _levels[_currentLevel].hasWood, _levels[_currentLevel].hasLighting);
        _waveCounter++;
        return mountain;
    }
    #endregion

    #region STRUCTS
    [System.Serializable]
    public struct MountainSprite
    {
        public string mountainType;
        public List<Sprite> sprites;
    }
    #endregion
}
