﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ScoreCount : MonoBehaviour {
    #region STATIC FIELDS
    public static float _score;
    #endregion

    #region CONST FIELDS
    const float _speed = 2f;
    const float _boostMultiplier = 2f;
    const float _eachLevelScore = 100f;
    #endregion

    #region PRIVATE FIELDS
    [SerializeField]
    private Text _levelScoreText;
    private Text _scoreText;
    private float _timeCounter =0;
    #endregion

    #region MONO BEHAVIOURS
    void Start () {
        _scoreText = GetComponent<Text>();
        StartCoroutine(LevelRoutine());
	}
	
	// Update is called once per frame
	void FixedUpdate () {

        _timeCounter += TimeManager.deltaTime * _speed * (CharacterState.Instance.GetState() != CharacterState.State.Boost ? 1 : _boostMultiplier);
        if (CharacterState.Instance.GetState() != CharacterState.State.NoMove )
        {
            _score = _timeCounter;
        }
        _scoreText.text = _score.ToString ("0");

	}
    #endregion

    #region PRIVATE METHODS
    IEnumerator LevelRoutine()
    {
        while (CharacterState.Instance.GetState() != CharacterState.State.Dead)
        {
            if (_timeCounter == 0 || (int)_timeCounter % _eachLevelScore == 0)
            {
                _levelScoreText.text = _timeCounter.ToString("0");
                _levelScoreText.gameObject.SetActive(true);
                yield return new WaitForSeconds(3f);
                _levelScoreText.gameObject.SetActive(false);
            }
          
            yield return null;
        }
    }
    #endregion

}
