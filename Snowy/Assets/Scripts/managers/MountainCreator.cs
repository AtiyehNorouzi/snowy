﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/*
 * Written by : Smoky Shadow
 * This script is responsible for generating mountain scene with obstacle and snowsets due to current level difficulties 
*/
public class MountainCreator
{
    #region PUBLIC METHODS
    public void GenerateMountain(Mountain m , List<Mountain.ObstacleStruct> obstaceTypes)
    {

        GenerateSnowSet(m.snowSetParent.transform.childCount, m.snowSetParent);
        GenerateObstacles(m, obstaceTypes);
      
    }
    #endregion

    #region PRIVATE METHODS
    private void GenerateSnowSet(int n, GameObject snowParent)
    {
        int type = 0;
        int random = GetSnowCount(snowParent.transform);
        for (int i = 0; i < random; i++)
        {
            int randChild = Random.Range(0, snowParent.transform.childCount);
            while (snowParent.transform.GetChild(randChild).childCount > 0)
                randChild = Random.Range(0, snowParent.transform.childCount);
            GenerateObject(PoolReference.GetRandomSnowSet(out type).GetObj(ObjectPool<GameObject>.GetSnowType(type)), snowParent, randChild);
        }
    }

    private void GenerateObstacles(Mountain m ,List<Mountain.ObstacleStruct> obstacleTypes)
    {
        //get random n obstacle size by getting stones parent child count
        int generateCount = GetRandomWeight(obstacleTypes[0].GoParent.transform.childCount , SceneControl.Instance.GetCurrentLevel());
        List<int> randIndexes = NUniqueNumbers(generateCount);
        int count = 10;
        for(int i =0; i < randIndexes.Count ; i++)
        {
            Mountain.ObstacleType obstacleType = GetRandomWeightedType(obstacleTypes);
            while (obstacleTypes[(int)obstacleType].GoParent.transform.childCount <= randIndexes[i] &&
                obstacleTypes[(int)obstacleType].GoParent.transform.childCount > 0 && count >=0)
            {
                count--;
                obstacleType = GetRandomWeightedType(obstacleTypes);
            }
            count = 10;
            GenerateObject(GetSpawnedObstacle(obstacleType) , obstacleTypes[(int)obstacleType].GoParent , randIndexes[i]);
        }
     
    }
    private Mountain.ObstacleType GetRandomWeightedType(List<Mountain.ObstacleStruct> obstacles)
    {
        int weightSum = 0;
        int index = 0;
        int lastIndex = obstacles.Count - 1;
        for (int i = 0; i < obstacles.Count; i++)
        {
            weightSum += obstacles[i].Weight;
        }
        while (index < lastIndex)
        {
            if (Random.Range(0, weightSum) < obstacles[index].Weight)
                return obstacles[index].Type;
            weightSum -= obstacles[index++].Weight;
        }
        return obstacles[index].Type;
    }

    private GameObject GetSpawnedObstacle(Mountain.ObstacleType obstacleType)
    {
        int type = 0;
        return obstacleType == Mountain.ObstacleType.Stone ? PoolReference.GetRandomStone(out type).GetObj(ObjectPool<GameObject>.GetStoneType(type)) :
                obstacleType == Mountain.ObstacleType.Kid ? PoolReference.kidInstance.GetObj(ObjectPool<KidMovement>.ObjType.Kid).gameObject :
                obstacleType == Mountain.ObstacleType.Machine ? PoolReference.machineInstance.GetObj(ObjectPool<MachineMovement>.ObjType.Machine).gameObject :
                PoolReference.machineInstance.GetObj(ObjectPool<MachineMovement>.ObjType.Machine).gameObject;
    }

    private List<int> NUniqueNumbers(int n)
    {
        List<int> randIndexes = new List<int>();
        int newNum = Random.Range(0, n);
        for (int i = 0; i < n; i++)
        {
            while (randIndexes.Contains(newNum))
                newNum = Random.Range(0, n);
            randIndexes.Add(newNum);
        }
        return randIndexes;

    }

    private void GenerateObject(GameObject go, GameObject parent , int index)
    {      
            go.transform.SetParent(parent.transform.GetChild(index).transform);
            go.transform.localPosition = Vector3.zero;
            go.transform.localRotation = Quaternion.identity;
    }

    private int GetRandomWeight(int total , int currentLevel)
    {
        return (currentLevel < 2) ?  Random.value < 0.6 ? Random.Range(0, total / 2) : Random.Range(total / 2, total) :
            Random.value < 0.65 ? Random.Range(0, total / 2) : Random.Range(total / 2, total);
    }

    private int GetSnowCount(Transform snowParent)
    {
        return Random.value < 0.6 ? Random.Range(0, snowParent.childCount / 2) : Random.Range(snowParent.transform.childCount / 2, 
           4);
    }
    #endregion

}
