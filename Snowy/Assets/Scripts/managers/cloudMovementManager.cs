﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cloudMovementManager : MonoBehaviour {
    /* 
    script for managing the cloud movements
    */
    #region sources
    [Header("cloud start transforms")]
    [Space(5)]
    public Transform StartPosition;
    public Transform MiddlePosition;
    public Transform EndPosition;
    Vector3 movedir;
    [Header("cloud prefab")]
    [Space(5)]
    public GameObject CloudPrefab;
    GameObject CloudLeaving;
    GameObject CloudComing;
    [Header("enemyPrefabs")]
    [Space(5)]
    public GameObject floatingEnemy;
    public GameObject Tigh;
    [Header("Coin Generating")]
    [Space(5)]
    public float speed = 5f;
    public GameObject coin;
    public Transform coinrect1;
    public Transform coinrect2;
    float frequency = 10f;//the speed of sin way move
    float magnitude = 0.5f;//the height of sin way move
    float time;
    float levelTime = 0;
    Vector3 position;//the position to move the coin up
    [System.Serializable]
    public class CloudLevel
    {
        public string name;
        public float time;
        public float velocity;
        public int maxEnemy;
        public int minEnemy;
    }
    public CloudLevel[] cloudlevels;
    int currentLevel = 2;
    void OnEnable()
    {
        ++currentLevel;
    }
    #endregion
    // Use this for initialization
    void Start () {
        //create two Clouds On up and Down Positions
        CloudComing = ObjectPooler.manager.GetPooledObject("Cloud");
        CloudComing.transform.position = StartPosition.position;
        CloudComing.transform.rotation = StartPosition.rotation;
        onSpawn(CloudComing.transform.GetChild(0).gameObject, 0);
        onSpawn(CloudComing.transform.GetChild(1).gameObject, 1);
        CloudComing.SetActive(true);
        CloudLeaving = ObjectPooler.manager.GetPooledObject("Cloud");
        CloudLeaving.transform.position = MiddlePosition.position;
        CloudLeaving.transform.rotation = MiddlePosition.rotation;
        onSpawn(CloudLeaving.transform.GetChild(0).gameObject, 0);
        onSpawn(CloudLeaving.transform.GetChild(1).gameObject, 1);
        CloudLeaving.SetActive(true);
        movedir = (MiddlePosition.position - StartPosition.position).normalized;
        position = coinrect1.position;
        levelTime = Time.time;

    }

    // Update is called once per frame
    void Update() {
        if (Time.time <= levelTime + cloudlevels[currentLevel].time)
        {
            //check if the mountainComing 's position is equal to the upstartPosition 
            if (Vector3.Distance(StartPosition.position, CloudComing.transform.position) >= Vector3.Distance(CloudComing.transform.position, EndPosition.position))
            {

                CloudLeaving = CloudComing;
                CloudLeaving.transform.position = MiddlePosition.position;
                CloudComing = ObjectPooler.manager.GetPooledObject("Cloud");
                CloudComing.transform.position = StartPosition.position;
                CloudComing.transform.rotation = StartPosition.rotation;
                onSpawn(CloudComing.transform.GetChild(0).gameObject, 0);
                onSpawn(CloudComing.transform.GetChild(1).gameObject, 1);
            }
        }
        else
        {
            if (CloudComing.transform.position.x <= EndPosition.position.x)
            {
               
             
                JetPackMove.keyboardlock = false;
                JetPackMove.finishedMove = true;
                enabled = false;
            }

        }

        #region coinGenerating
        Vector3 sin = Vector3.up * Mathf.Sin(Time.time * frequency) * magnitude;
            position += Vector3.right * Time.deltaTime * 2f;

            if (Time.time >= time + 0.1f)
            {

                GameObject coin = ObjectPooler.manager.GetPooledObject("onaircoin");

                coin.transform.position = position + sin;
                coin.GetComponent<Rigidbody2D>().isKinematic = true;
                coin.SetActive(true);
                time = Time.time;
                coin.GetComponent<Rigidbody2D>().velocity = Vector3.left * speed;
            }
            
        
       
        #endregion


    }
    /* spawn the enemys of the cloud*/
    void onSpawn(GameObject cloud , int upOrDown)
    {
        switch (currentLevel)
        {
         //just coin
            case 0:
                {
                    //do nothing
                    break;
                }
            //just coin with Tigh mane
        
            case 1:
                {
                    int randomNumber = Random.Range(cloudlevels[currentLevel].minEnemy, cloudlevels[currentLevel].maxEnemy);
                    setTighs(randomNumber , cloud , upOrDown);
                    break;
                }
            //coin with Tigh Mane and floating enemy
       
            case 2:
                {
                    int randNumber = Random.Range(cloudlevels[currentLevel].minEnemy, cloudlevels[currentLevel].maxEnemy);
                    setFloatingEnemyOrTighs(randNumber, cloud, upOrDown);
                    break;
                }
            //coin with Tigh mane and FLoating enemy and Tigh moving Up and Down
            case 3:
                {
                    int rnumber = Random.Range(cloudlevels[currentLevel].minEnemy, cloudlevels[currentLevel].maxEnemy);
                    setFloatingEnemyOrTighs(rnumber, cloud, upOrDown);
                    break;
                }

        }
    }
    void setTighs(int spawnNumber , GameObject obj , int upOrdown)
    {
        int random;
        //get the locations
        int Tlength = obj.transform.childCount;
        List<int> CreatedTighs = new List<int>(spawnNumber);
        random = Random.Range(0, Tlength);
        for (int i = 0; i < spawnNumber; i++)
        {
            if (i == 0)
            {
                random = Random.Range(0, Tlength);
            }
            else {
                while (CreatedTighs.Contains(random))
                {
                    random = Random.Range(0, Tlength);
                }
            }

            GameObject TighGO = ObjectPooler.manager.GetPooledObject("metalTigh");
            TighGO.transform.SetParent(obj.transform.GetChild(random).transform);
            TighGO.transform.localPosition = Vector3.zero;
            TighGO.transform.localRotation = Quaternion.identity;
            CreatedTighs.Add(random);
            random = Random.Range(0, Tlength);
        }
 
    }
    void setFloatingEnemyOrTighs(int spawnNumber, GameObject obj, int upOrdown)
    {
        int random;
        //get the locations
        int Tlength = obj.transform.childCount;
        List<int> CreatedTighs = new List<int>(spawnNumber);
        random = Random.Range(0, Tlength);
        for (int i = 0; i < spawnNumber; i++)
        {
            if (i == 0)
            {
                random = Random.Range(0, Tlength);
            }
            else {
                while (CreatedTighs.Contains(random))
                {
                    random = Random.Range(0, Tlength);
                }
            }
            if (upOrdown == 0)
            {
                GameObject enemyGO;
                int rand = Random.Range(0, 2);
                if (rand == 0)
                {
                    enemyGO = ObjectPooler.manager.GetPooledObject("FloatingEnemy");

                    enemyGO.GetComponent<DistanceJoint2D>().connectedBody = obj.transform.GetChild(random).GetComponent<Rigidbody2D>();
                }
                else
                {
                    enemyGO = ObjectPooler.manager.GetPooledObject("metalTigh");
                    if(currentLevel == 3)
                    {
                        enemyGO.GetComponent<Animator>().enabled = true;
                      
                    }
                }

                enemyGO.transform.SetParent(obj.transform.GetChild(random).transform);
                enemyGO.transform.localPosition = Vector3.zero;
                enemyGO.transform.localRotation = Quaternion.identity;

            }
            else {
                GameObject TighGO = ObjectPooler.manager.GetPooledObject("metalTigh");
                TighGO.transform.SetParent(obj.transform.GetChild(random).transform);
                if (currentLevel == 3)
                {
                    TighGO.GetComponent<Animator>().enabled = true;
                }
                TighGO.transform.localPosition = Vector3.zero;
                TighGO.transform.localRotation = Quaternion.identity;
            }
            CreatedTighs.Add(random);
            random = Random.Range(0, Tlength);
        }
    }
}
