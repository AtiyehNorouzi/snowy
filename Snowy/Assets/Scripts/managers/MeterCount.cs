﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
//meter counter script
public class MeterCount : MonoBehaviour {
	float time;
	float currenttime;
	public static float meter;
	float speed=5f;
	float scoreTime;
	public GameObject score;
	float levelscore=0f;
	bool check=true;
	bool check2=true;
	// Use this for initialization
	void Start () {
        
        time = Time.time;
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        //meter count
		currenttime = Time.time;

        //if (Character._instance.state == Character.State.NORMAL)
        //{
        //    meter = (currenttime - time) * speed ;
        //}

        this.GetComponent<Text> ().text = meter.ToString ("0.0");

        //if meter is the next level
		if (meter >= levelscore) {
			if(check){
			scoreTime=Time.time;
            //set the level counter text
			score.GetComponent<Text>().text=levelscore.ToString();
           //  MountainPrebabMove.velocity = true;
			score.SetActive(true);
			check=false;
			check2=true;
			}
			
			if(Time.time >= scoreTime + 3f && check2){
				score.SetActive(false);
				levelscore += 200f;
				check2=false;
				check=true;
			}
		}
	}
}
