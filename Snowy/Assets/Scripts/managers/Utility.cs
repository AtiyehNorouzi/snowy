﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Utility : MonoBehaviour {

    #region STATIC METHODS
    public static bool AlmostEqual(Vector3 a , Vector3 b , float dist)
    {
        if(Vector3.Distance(a , b) <= dist)
            return true;
        return false;
    }

    #endregion
}
