﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.SceneManagement;

public class PauseScript : MonoBehaviour {
    #region FIELDS
    [SerializeField]
	private GameObject _resumeBtn;
    [SerializeField]
    private GameObject _backMenuBtn;
    [SerializeField]
    private GameObject _retryBtn; 
    private CharacterMovement _character;
    #endregion

    #region MONO BEHAVIOURS
    // Use this for initialization
    void Start () {
        _character = GameObject.FindGameObjectWithTag("Gole").GetComponent<CharacterMovement>();
	}
    #endregion

    #region PUBLIC METHODS
    public void pause(){
        TimeManager.timeScale = 0;
        _character.FreezeCharacter();
        _character.transform.position += Vector3.up * 2f;
        OpenPanel(true);
    }
	public void Resume(){
        TimeManager.timeScale = 1;
        _character.DeFreezeCharacter();
        OpenPanel(false);
    }
    public void retry()
    {
        TimeManager.timeScale = 1;
        SoundPlayer.Instance.PlaySound(SoundPlayer.SoundClip.Click);
        SceneManager.LoadScene(1);
    }
    public void backtomenu()
    {
        TimeManager.timeScale = 1;
        Invoke("BackToMenu", 0.5f);
    }
    #endregion

    #region PRIVATE METHODS
    private void BackToMenu()
    {
        SceneManager.LoadScene(0);
    }
    private void OpenPanel(bool check)
    {
        _resumeBtn.SetActive(check);
        _backMenuBtn.SetActive(check);
        _retryBtn.SetActive(check);
        SoundPlayer.Instance.PlaySound(SoundPlayer.SoundClip.Click);
    }
    #endregion

}
