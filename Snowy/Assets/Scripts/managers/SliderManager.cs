﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
/*
script for manage the slider color and define the essential points
*/
public class SliderManager : MonoBehaviour
{
    #region FIELDS
    private static SliderManager _instance;
    [SerializeField]
    private Image _image;
    [SerializeField]
    private Color _BoostColor;
    [SerializeField]
    private Color _normalColor;
    [SerializeField]
    private Color _hintColor;
    #endregion

    #region ENUMS
    public enum ColorType  { Normal, Boost, Hint }
    #endregion

    #region PROPERTIES
    public static SliderManager Instance
    {
        get
        {
            if (_instance == null)
                _instance = GameObject.FindObjectOfType<SliderManager>();
            return _instance;
        }
    }
    #endregion

    #region PUBLIC METHODS
    public void SetColor(ColorType type)
    {
        _image.color = type == ColorType.Boost ? _BoostColor : type == ColorType.Hint ? _hintColor : _normalColor;
    }
    #endregion
}
