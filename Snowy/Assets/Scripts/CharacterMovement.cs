﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/*
 * Character movement
 * this script contains movement of the character (jump, running, doublejump)
 * written by Smokey Shadow
 */
public class CharacterMovement : MonoBehaviour {

    #region CONST FIELDS
    float rotateDegree = -360;
    const float MAX_VELOCITY = 3;
    #endregion

    #region FIELDS

    [SerializeField]
    private Rigidbody2D _character_rb;

    private bool _canDoubleJump;

    private RaycastHit2D _hit;

    private bool _firstJump = false;

    #endregion

    #region MONOBEHABIOUR
    void FixedUpdate () {
     
        if(CharacterState.Instance.GetState() == CharacterState.State.Running)
            _character_rb.velocity = Vector3.ClampMagnitude(_character_rb.velocity, MAX_VELOCITY);

        if (CharacterState.Instance.GetState() == CharacterState.State.Boost)
            return;


        //rotateDegree -= TimeManager.deltaTime;

      
       // _character_rb.MoveRotation(rotateDegree * 50*  SceneControl.Instance.GetCurrentVelocity());


        _hit = Physics2D.Raycast (transform.position, -Vector2.up, 0.7f, LayerMask.GetMask ("mountain"));

 
        if (_hit.collider != null) {
            if (Input.GetKeyDown(KeyCode.Space) || Input.GetMouseButtonDown(0))
            {
                _character_rb.AddForce(Vector3.up * 10f , ForceMode2D.Impulse);
                _canDoubleJump = true;
                CharacterState.Instance.ChangeState(CharacterState.State.Jump);
                if (!_firstJump)
                {
                    _firstJump = true;
                    _character_rb.constraints = RigidbodyConstraints2D.None;
                }
            }
            else
            {

                //note ta bere bala momkene raycast bokhore va kam tar bashe va bere tu state running dobare
                if (CharacterState.Instance.GetState() != CharacterState.State.Running && _firstJump && _character_rb.velocity.y <= 0)
                    CharacterState.Instance.ChangeState(CharacterState.State.Running);
            }
        } else {
            if (Input.GetKeyDown (KeyCode.Space) || Input.GetMouseButtonDown (0)) {
                if (_canDoubleJump) {
                    _character_rb.AddForce (Vector3.up * 17f, ForceMode2D.Impulse);
                    _canDoubleJump = false;
                }
            }
        }

   

    }
    #endregion
}