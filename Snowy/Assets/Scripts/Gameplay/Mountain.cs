﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/*
 * Written by : Smoky Shadow
 * This script is for mountain movement and entities
*/
public class Mountain : MonoBehaviour
{
    #region ENUM 
    public enum ObstacleType { Stone =0, Kid , Machine , Glider};
    #endregion

    #region FIELDS
    [SerializeField]
    private SpriteRenderer sr;

    [SerializeField]
    private int type;

    [SerializeField]
    private ObstacleStruct [] obstacles;

    private MountainCreator _mountainCreator = new MountainCreator();

    public GameObject snowSetParent;


    #endregion

    #region PROPERTIES
    public SpriteRenderer SpRenderer
    {
        get
        {
            return sr;
        }
    }
    #endregion

    #region PUBLIC METHODS

    public void CreateMountain(int enemyCount, bool hasStone, bool hasKid, bool hasMachinee, bool hasGlider, bool hasLighting)
    {
        List<ObstacleStruct> list;
        LevelObstacles(out list, hasStone, hasKid, hasMachinee, hasGlider);
        if (list.Count > 0)
            _mountainCreator.GenerateMountain(this , list);
    }
    public void SetSprite(Sprite sprite)
    {
        sr.sprite = sprite;
    }
  
    public void Despawn()
    {
        PoolReference.GetMountainIsntance(type).PushObj(ObjectPool<Mountain>.GetMountainType(type), this);
    }
    #endregion

    #region PRIVATE METHODS
    private void LevelObstacles(out List<ObstacleStruct> list , bool hasStone, bool hasKid, bool hasMachinee, bool hasGlider)
    {
        list = new List<ObstacleStruct>();
        if (hasStone)
            list.Add(obstacles[0]);
        if (hasKid)
            list.Add(obstacles[1]);
        if (hasMachinee)
            list.Add(obstacles[2]);
        if (hasGlider)
            list.Add(obstacles[3]);
    }
    #endregion

    #region STRUCTS
    [System.Serializable]
    public struct ObstacleStruct
    {
        public int Weight;
        public ObstacleType Type;
        public GameObject GoParent;
    }
    #endregion

}
