﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterParticleControl 
{
    #region FIELDS
    private static CharacterParticleControl _instance;

    [SerializeField]
    private ParticleSystem[] particles;

    private ParticleType _particleType;

    private float _playTimer = 0;
    #endregion


    #region ENUM
    public enum ParticleType { JumpUp = 0, JumpDown, OnCollideSnow, OnCollideSheild, Die, Boost }
    #endregion

    #region PROPERTIES
    public static CharacterParticleControl Instance
    {
        get
        {
            if (_instance == null)
                _instance = new CharacterParticleControl();
            return _instance;
        }
    }

    #endregion

    public void PlayParticle(ParticleType type , int playTime = 0)
    {
        particles[(int)type].Play();
        if(playTime > 0 )
        {
            _playTimer += TimeManager.deltaTime / playTime;
            if(_playTimer >= 1)
            {
                _playTimer = 0;
                particles[(int)type].Stop();
            }
        }
    }
}
