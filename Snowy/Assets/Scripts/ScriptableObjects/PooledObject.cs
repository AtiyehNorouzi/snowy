﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/*
 * Written by : Smoky Shadow
 * This script is  Pool object Data Structure
*/
public class PooledObject : ScriptableObject
{
    public new string name;
    public GameObject prefab;
    public int amount;
    public int amountNow;
}
