﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level : ScriptableObject
{
    public float WaveNumbers;
    public float Velocity;
    public int minEnemies;
    public int maxEnemies;
    public bool hasStone;
    public bool hasKid;
    public bool hasJet;
    public bool hasMachine;
    public bool hasLighting;
    public bool hasWood;
}
