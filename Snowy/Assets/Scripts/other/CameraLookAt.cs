﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/*
 * Written by : Smoky Shadow
 * this script is for following a target by camera in a smooth way 
*/
public class CameraLookAt : MonoBehaviour {

    #region CONST FIELDS
    private const float smoothTime = 0.5f;
    private const float zOffset = -10;
    #endregion

    #region FIELDS
    [SerializeField]
    private Transform target;
    private Vector3 offset;
    private Vector3 velocity = Vector3.zero;
    #endregion

    #region MONO BEHAVIOUR
    void Start()
    {
        offset = new Vector3(transform.position.x - target.position.x, 0 , zOffset);
    }

	void LateUpdate () {

        transform.position = Vector3.SmoothDamp(transform.position, target.position + offset , ref velocity, smoothTime);
    }
    #endregion
}
