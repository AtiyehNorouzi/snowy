﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/*this script is for animating frame by frame images */
public class animatetextures : MonoBehaviour {
   
    #region sources
    public Sprite[] sprites;
    public float framesPerSecond = 2f;
    private SpriteRenderer spriteRenderer;
    #endregion
    // Use this for initialization
    void Start () {
        spriteRenderer = GetComponent<Renderer>() as SpriteRenderer;
        #region deadBallAnimate
        if (this.gameObject.tag != "kid" )
        {
            Invoke("gotoHighscore", 1f);
        }
        #endregion
    }

    // Update is called once per frame
    void Update () {
        int index = (int)(Time.timeSinceLevelLoad * framesPerSecond);
        index = index % sprites.Length;
        spriteRenderer.sprite = sprites[index];
    }
    void gotoHighscore()
    {
        Application.LoadLevel("Highscore");
    }
}
