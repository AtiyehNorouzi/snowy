﻿using UnityEngine;
using System.Collections;
/*
 * Written by : Smoky Shadow
 * This script is for scrolling Renderers
*/
public class scroll : MonoBehaviour
{
    #region FIELDS
    [SerializeField]
    public Renderer rd;
    [SerializeField]
    public float speed;
    #endregion

    #region MONO BEHAVIOURS
    void Update () {
       
        rd.material.mainTextureOffset = new Vector2 (Time.time * speed, 0f);

	}
    #endregion
}
