﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrowSnow : MonoBehaviour
{
    private float _timeCounter = 0;
    private Material _roofMat;
    // Start is called before the first frame update
    void Start()
    {
        _roofMat = GetComponent<Renderer>().materials[1];
    }

    // Update is called once per frame
    void Update()
    {

        if (SunRotate._isSnow)
        {
            if (_roofMat.GetFloat("_Snow") >= -0.9)
            {

                _timeCounter -= TimeManager.deltaTime * 0.4f;
                _roofMat.SetFloat("_Snow", Mathf.Clamp(_timeCounter, -1, 1));
                if (_timeCounter <= -1)
                    _timeCounter = 0;
            }

        }
        else
        {
            if (_roofMat.GetFloat("_Snow") <= 0f)
            {
                _timeCounter += TimeManager.deltaTime * 0.4f;
                _roofMat.SetFloat("_Snow", Mathf.Clamp(_timeCounter, -1, 1));
            }
            else
            {
                _timeCounter = 0;
            }
        }
    }
}
