﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeBlend : MonoBehaviour
{
    [SerializeField]
    private Material _blendMat;
    private float _timeCounter = 0;
    [SerializeField]
    private Texture2D[] _textures;
    private int _textureIterator = 0;
    private float dir = 1;
    // Update is called once per frame
    private void Start()
    {
        _blendMat.SetTexture("_FrontTex", _textures[_textureIterator]);
        _blendMat.SetTexture("_FrontTex2", _textures[_textureIterator+1]);

        _blendMat.SetTexture("_BackTex", _textures[_textureIterator]);
        _blendMat.SetTexture("_BackTex2", _textures[_textureIterator+1]);

        _blendMat.SetTexture("_LeftTex", _textures[_textureIterator]);
        _blendMat.SetTexture("_LeftTex2", _textures[_textureIterator+1]);

        _blendMat.SetTexture("_RightTex", _textures[_textureIterator]);
        _blendMat.SetTexture("_RightTex2", _textures[_textureIterator+1]);

        _blendMat.SetTexture("_DownTex", _textures[_textureIterator]);
        _blendMat.SetTexture("_DownTex2", _textures[_textureIterator + 1]);

        _textureIterator += 2;
    }
    void Update()
    {
        if (_textureIterator >= _textures.Length)
            _textureIterator = 0;
        _timeCounter += TimeManager.deltaTime * 0.1f * dir;
        _blendMat.SetFloat("_Blend", Mathf.Clamp(_timeCounter, 0 , 1));
        if (_timeCounter >= 1)
        {
            dir *= -1;
            _blendMat.SetTexture("_FrontTex", _textures[_textureIterator]);
            _blendMat.SetTexture("_BackTex", _textures[_textureIterator]);
            _blendMat.SetTexture("_LeftTex", _textures[_textureIterator]);
            _blendMat.SetTexture("_RightTex", _textures[_textureIterator]);
            _blendMat.SetTexture("_DownTex", _textures[_textureIterator++]);
        }
        if (_timeCounter <= 0)
        {
            dir *= -1;
            _blendMat.SetTexture("_FrontTex2", _textures[_textureIterator]);
            _blendMat.SetTexture("_BackTex2", _textures[_textureIterator]);
            _blendMat.SetTexture("_LeftTex2", _textures[_textureIterator]);
            _blendMat.SetTexture("_RightTex2", _textures[_textureIterator]);
            _blendMat.SetTexture("_DownTex2", _textures[_textureIterator++]);
        }
    }
}
