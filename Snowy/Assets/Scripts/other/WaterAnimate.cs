﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterAnimate : MonoBehaviour
{
    [SerializeField]
    private SpriteRenderer _sr;
    // Start is called before the first frame update
    void OnEnable()
    {
        StartCoroutine(SpriteAnimationRoutine(GameAssetRefs.Instance.WaterSprites, _sr));
    }

    static IEnumerator SpriteAnimationRoutine(List<Sprite> sprites, SpriteRenderer _spRenderer)
    {
        int i;
        i = 0;
        while (true)
        {
            if (i < sprites.Count)
            {
                _spRenderer.sprite = sprites[i];
                i++;
                if (i >= sprites.Count)
                    i = 0;
                yield return new WaitForSeconds(0.15f);
            }

        }

    }

}
