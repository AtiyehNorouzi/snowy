﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class IntroScript : MonoBehaviour {
    #region FIELDS
    private AsyncOperation result;
    public GameObject deactiveOnloading;
    public GameObject tobeactivate;
    #endregion

    #region MONO BEHAVIOURS
    // Use this for initialization
    IEnumerator Start () {
        result = SceneManager.LoadSceneAsync("snowball");
        result.allowSceneActivation = false;
       
        while (!result.isDone)
        {
            if(result.progress >= 0.9f)
            {
                Debug.Log("progress" + result.progress);
                yield return new WaitForSeconds(2f);
                Activate();
                StopCoroutine("Start");
            }

            yield return new WaitForEndOfFrame();

        }
  

    }
    #endregion

    #region PUBLIC METHODS

    public void playgame()
    {
        SceneManager.UnloadScene("Start");
        result.allowSceneActivation = true;
    }
    #endregion

    #region PRIVATE METHODS
    private void Activate()
    {
        deactiveOnloading.SetActive(false);
        tobeactivate.SetActive(true);
        CancelInvoke();
    }
    #endregion

}
