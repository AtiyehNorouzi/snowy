﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class SwipeDetector : MonoBehaviour
{
    #region FIELDS
    [SerializeField]
    private GameObject [] _pages;
    private bool _onChangingScene;
    private GameObject _currentScene;
    private float _changeSceneAlpha;
    #endregion

    #region MONO BEHAVIOURS
    void Start()
    {
        _currentScene = _pages[0];
    }
    void Update()

    {
        if (_onChangingScene)
        {
            if (_changeSceneAlpha > 0)
            {
                _changeSceneAlpha = Mathf.Lerp(_changeSceneAlpha, 0, TimeManager.deltaTime * 1 / _changeSceneAlpha);
                _currentScene.GetComponent<Image>().color = new Color(1, 1, 1, _changeSceneAlpha);
            }
            else
            {
                _currentScene.SetActive(false);
                _currentScene.GetComponent<Image>().color = new Color(1, 1, 1, 1);
                _onChangingScene = false;
            }
        }
        
    }
    #endregion

    #region PUBLIC METHODS
    public void OnRight()
    {
        if (_pages[0].gameObject.activeInHierarchy)
        {
            _changeSceneAlpha = 1;
            _onChangingScene = true;

        }
        else if (_pages[1].gameObject.activeInHierarchy)
        {
            _changeSceneAlpha = 1;
            _currentScene = _pages[1];
            _onChangingScene = true;

        }
    }

    public void OnLeft()
    {
        if (_pages[1].gameObject.activeInHierarchy)
        {
            _pages[0].gameObject.SetActive(true);

            return;
        }
        if (_pages[2].gameObject.activeInHierarchy)
        {
            _pages[1].gameObject.SetActive(true);

        }
    
    }
    #endregion

}
