﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class swipedetecter : MonoBehaviour
{


    public GameObject [] pages;
    bool onchangingScene;
    GameObject currentScene;
    float changeSceneAlpha;
    void Start()
    {
        currentScene = pages[0];
    }
    void Update()

    {
        if (onchangingScene)
        {
            if (changeSceneAlpha > 0)
            {
                changeSceneAlpha = Mathf.Lerp(changeSceneAlpha, 0, TimeManager.deltaTime * 1 / changeSceneAlpha);
                currentScene.GetComponent<Image>().color = new Color(1, 1, 1, changeSceneAlpha);
            }
            else
            {
                currentScene.SetActive(false);
                currentScene.GetComponent<Image>().color = new Color(1, 1, 1, 1);
                onchangingScene = false;
            }
        }
        
    }
    public void Onright()
    {
        if (pages[0].gameObject.activeInHierarchy)
        {
            changeSceneAlpha = 1;
            onchangingScene = true;

        }
        else if (pages[1].gameObject.activeInHierarchy)
        {
            changeSceneAlpha = 1;
            currentScene = pages[1];
            onchangingScene = true;

        }
   

    }
    public void OnLeft()
    {
        if (pages[1].gameObject.activeInHierarchy)
        {
            pages[0].gameObject.SetActive(true);

            return;
        }
        if (pages[2].gameObject.activeInHierarchy)
        {
            pages[1].gameObject.SetActive(true);

        }
    
    }

}
