﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using System;
/*script for storing the highscore in a json file and retry and menu controller*/

public class HighScoreStore : MonoBehaviour {
    //store all json file text in variable
    public static string json;
    // Use this for initialization
    [System.Serializable]
    public class AllScores
    {
        public HighScore[] scores = new HighScore[5];
    }
    //highscore class that implements Comparable for sorting by the score attribute
    [System.Serializable]
    public class HighScore : IComparable
    {
        public float score;
        public string time;
        //compare the array with scores
        public int CompareTo(object obj)
        {
            if(obj is HighScore)
            {
                return (obj as HighScore).score.CompareTo(this.score);
            }
            throw new NotImplementedException();
        }
    }

	// Use this for initialization
	void Start () {
        //set Highscore Page text to Meter Count 
        transform.GetChild(2).gameObject.GetComponent<Text>().text = ((int)MeterCount.meter).ToString();
        saveHighScore();
      
    }
    void saveHighScore()
    {

        //write objects on json file
        //json path
        string jsonstring;
        //get the json path to write
        string path = Application.dataPath + "/StreamingAssets/highscores.json";
        if (File.Exists(path))
        {
            json = File.ReadAllText(path);
        }
        //initialize the array the read stored datas from the json file  , new scores and set them two zero
        HighScore[] scores = new HighScore[5];
        for (int i = 0; i < 5; i++)
        {
            scores[i] = new HighScore();

         }
        //get the AllScores object from json
        AllScores usedScores = JsonUtility.FromJson<AllScores>(json);
        //if its null : when the first score wants to store set it to scores
        if (usedScores.scores == null)
        {
            usedScores.scores = scores;
        }
        //for get the empty element of array 
        int index = -1;
        //for get the min number of the array
        float min = usedScores.scores[0].score;
        //for find the minimum index of array elements
        int minIndex=0;
        if (usedScores != null)
        {
            //set the previous stored numbers to scores array

            for(int i = 0; i < 5; i++)
            {
                //check if we have an empty array house and set the index to that location
                if(usedScores.scores[i].score == 0)
                {
                    index = i;
                }
                // if the index's element fill with number
                if(usedScores.scores[i].score != 0)
                {
                    //check if previous number is smaller that min (for finding minimum)
                    if(usedScores.scores[i].score <= min)
                    {
                        //reset minimum and minIndex 
                        min = usedScores.scores[i].score;
                        minIndex = i;
                    }
                    //set i locations element to previous number stored
                    scores[i] = usedScores.scores[i];
                }
            }
            //if we have an empty hosue set new number to the index 's hosue
            if (index != -1)
            {
                scores[index].score = (int)MeterCount.meter;
                scores[index].time = System.DateTime.Now.ToString();
            }
            //if all houses filled with number , if the number is  greator than the minimum set minimum to that number
            else
            {
                if((int)MeterCount.meter >= min)
                {
                    scores[minIndex].score = (int)MeterCount.meter;
                    scores[minIndex].time = System.DateTime.Now.ToString();
                }

            }

        }
 

        AllScores s = new AllScores();    
        //sort array decreasing as scores
        Array.Sort(scores);
        s.scores = scores;
        //convert object to json style
        jsonstring = JsonUtility.ToJson(s);
        //write string on path
        TextWriter writer = new StreamWriter(path, false);
        writer.WriteLine(jsonstring);
        writer.Close();
    }
    void OnRetryClick()
    {
        Application.LoadLevel("snowball");
    }
    void onMenuClick()
    {
        Application.LoadLevel("Start");
    }

}
