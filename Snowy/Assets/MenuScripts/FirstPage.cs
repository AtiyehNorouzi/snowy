﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirstPage : MonoBehaviour {
    float time;
    public GameObject the;
    public GameObject downtrodden;
    public GameObject Menu;
    public GameObject logo;
    public Camera cam;
	// Use this for initiaization
	void Start () {
        time = Time.time;
	}
	
	// Update is called once per frame
	void Update () {
		if(Time.time > 0.5 + time)
        {
           the.SetActive(true);
        }
        if(Time.time >2 + time)
        {
            downtrodden.SetActive(true);
        }
        if(Time.time >3 + time)
        {
            downtrodden.transform.parent.gameObject.SetActive(false);
            logo.SetActive(true);
        }
        if(Time.time > 4 + time)
        {
            logo.SetActive(false);
            Menu.SetActive(true);
            cam.gameObject.SetActive(true);
        }
       
	}
}
