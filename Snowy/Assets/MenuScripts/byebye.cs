﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
/*script for quit the game after five seconds */
public class byebye : MonoBehaviour {

    float Timer;
    
	// Use this for initialization
	void Start () {
        Timer = Time.time;
	}

    // Update is called once per frame
    void Update()
    {
  
        if (Time.time >= Timer + 5f)
        {
            Application.Quit();
        }
        
    }
}
