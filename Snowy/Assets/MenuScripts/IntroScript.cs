﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class IntroScript : MonoBehaviour {
    #region scenes
    private AsyncOperation result;
    public GameObject deactiveOnloading;
    public GameObject tobeactivate;
    #endregion
    // Use this for initialization
    IEnumerator Start () {
        result = SceneManager.LoadSceneAsync("snowball");
        result.allowSceneActivation = false;

        while (!result.isDone)
        {
            Debug.Log("progress" + result.progress);
            yield return new WaitForEndOfFrame();
            if(result.progress >= 0.9f)
            {
                Invoke("activate", 2f);
                StopCoroutine("Start");
            }
            
        }
  

    }
	void activate()
    {
        Debug.Log("in activate");
        deactiveOnloading.SetActive(false);
        tobeactivate.SetActive(true);
        CancelInvoke();
    }

    public void playgame()
    {
        SceneManager.UnloadScene("Start");
        result.allowSceneActivation = true;
    }
}
