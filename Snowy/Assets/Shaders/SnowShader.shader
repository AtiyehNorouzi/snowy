﻿// Upgrade NOTE: replaced '_World2Object' with 'unity_WorldToObject'


Shader "ShaderLearn/SnowShader"
{
	Properties
	{


		_MainColor("main color" , Color) = (1.0,1.0,1.0,1.0)
		_MainTex("Texture" , 2D) = "white"{}
		_Bump("Normal" , 2D) = "bump"{}
		_Snow("snow dir threshold" , Range(-1 , 1)) = 1
		_SnowDirection("direction" , Vector) = (1,1,1,1)
		_SnowDepth("snow depth" , Range(0,1)) = 0
		_SnowColor("snow color" , Color) = (1.0,1.0,1.0,1.0)
	}
		SubShader
	{
		Tags
	{
		"RenderType" = "Opaque"
	}
		LOD 200
		CGPROGRAM
#pragma surface surf Standard  vertex:vert
#pragma target 3.0
		struct Input
	{
		float2 uv_MainTex;
		float2 uv_Bump;
		float3 worldNormal;
		INTERNAL_DATA
	};
	sampler2D _MainTex;
	sampler2D _MyBump;
	float4 _MainColor;
	float4 _SnowColor;
	float _Snow;
	float4 _SnowDirection;
	float _SnowDepth;

	void vert(inout appdata_full v)
	{
		float4 sn = mul(_SnowDirection, unity_WorldToObject);
		if (dot(v.normal, sn.xyz) >= _Snow)
			v.vertex.xyz += (sn.xyz + v.normal) * _SnowDepth * _Snow;
	}
	void surf(Input IN , inout SurfaceOutputStandard o)
	{

		half4 c = tex2D(_MainTex, IN.uv_MainTex);
		o.Normal = UnpackNormal(tex2D(_MyBump, IN.uv_Bump));
		if (dot(WorldNormalVector(IN, o.Normal), _SnowDirection.xyz) >= _Snow)
			o.Albedo = _SnowColor.rgb;
		else
			o.Albedo = c.rgb * _MainColor;
		o.Alpha = 1;
	}
	half4  LightingSimpleLambert(SurfaceOutput s , half3 lightDir , half atten)
	{
		half ndotl = dot(s.Normal , lightDir);
		half4 c;
		c.rgb = s.Albedo * (_LightColor0.rgb*(ndotl *atten));
		c.a = s.Alpha;
		return c;
	}

	ENDCG
	}
		Fallback "Diffuse"
}