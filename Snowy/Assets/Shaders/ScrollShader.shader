﻿Shader "Downtrodden/ScrollingShader"
{
	Properties
	{
		_Color("Color", Color) = (1,1,1,1)
		_MainTex("Albedo (RGB)", 2D) = "white" {}
	_ScrollXSpeed("X Speed " , Range(0,10)) = 2
		_ScrollYSpeed(" Y Speed" , Range(0,10)) = 2
	}
		SubShader
	{
		Tags{ "RenderType" = "Opaque" }
		LOD 200

		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
#pragma surface surf Lambert fullforwardshadows

		// Use shader model 3.0 target, to get nicer looking lighting
#pragma target 3.0

		sampler2D _MainTex;

	struct Input
	{
		float2 uv_MainTex;
	};


	fixed4 _Color;
	fixed _ScrollXSpeed;
	fixed _ScrollYSpeed;


	void surf(Input IN, inout SurfaceOutput o)
	{

		fixed2 uv = IN.uv_MainTex;

		fixed xScrollVal = _ScrollXSpeed * _Time;
		fixed yScrollVal = _ScrollYSpeed * _Time;

		uv += fixed2(xScrollVal, 0);

		half4 c = tex2D(_MainTex, uv);
		o.Albedo = c.rgb * _Color;
		o.Alpha = c.a;
	}
	ENDCG
	}
		FallBack "Diffuse"
}
